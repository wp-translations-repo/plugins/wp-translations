<?php
/**
 * Fired when the plugin is uninstalled.
 *
 * @author     WP-Translations Team
 * @link       https://wp-translations.org
 * @since      1.0.3
 *
 * @package    WP_Translations
 */

// Exit if accessed directly.
if ( ! defined( 'WP_UNINSTALL_PLUGIN' ) ) exit;

global $wpdb;

// Options
delete_site_option( 'wpt_settings' );
delete_site_option( 'wpt_repositories' );
delete_site_option( 'wpt_logs' );
delete_site_option( 'wpt_translations' );
delete_site_option( 'wpt_version' );

// Transients
$wpdb->query( "DELETE FROM $wpdb->options WHERE option_name LIKE '\_site\_transient\_wpt\_%'" );
delete_site_transient( 'wp_translations_plugin_update' );
