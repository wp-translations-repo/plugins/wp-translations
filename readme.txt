=== WP-Translations ===
Contributors: WP-Translations Team, G3ronim0, fxbenard
Tags: Translations, i18n, Translate, Localization, l10n
Requires at least: 4.6
Tested up to: 4.9.8
Requires PHP: 5.6
Stable tag: 1.0.11
License: GPL V2

The easiest way to improve WordPress localization features.

== Description ==

WP-Translations is a complete translation solution for improving localization on WordPress. Learn more at [WP-Translations.org](https://wp-translations.org).

= Go further with Premium translations =

With our dedicated plugin from our [WP-Translations store](https://wp-translations.store) you can get translations for your premium WordPress plugins or themes.


= Built with developers in mind =

Extensible, adaptable, and open source -- WP-Translations is created with developers in mind. Contribute on [GitLab](https://gitlab.com/...).


== Installation ==

= 1. The easy way =
* Download the plugin (.zip file) by using the blue "download" button underneath the plugin banner at the top
* In your WordPress dashboard, navigate to Plugins > Add New
* Click on "Upload Plugin"
* Upload the .zip file
* Activate the plugin
* A new `Translations` menu is available in your WordPress dashboard

= 2. The old-fashioned and reliable way (FTP) =
* Download the plugin (.zip file) by using the blue "download" button underneath the plugin banner at the top
* Extract the archive and then upload, via FTP, the `wp-translations-pro` folder to the `<WP install folder>/wp-content/plugins/` folder on your host
* Activate the plugin
* A new `Translations` menu is available in your WordPress dashboard

== Changelog ==

= 1.0.9 =
* Enhancement: i18n strings review

= 1.0.8 =
* Enhancement: i18n strings review
* Bugfix: Whitescreen with front userlocale
* Update: .pot file

= 1.0.7 =
* Enhancement: Repository add/edit form

= 1.0.6 =
* Feature: Custom File
* Enhancement: DataTable in update-core
* Bugfix: licenses notices

= 1.0.5 =
* Feature: Languages management
* Rewrite page class
* Bugfix multiple php warning

= 1.0.4 =
* Feature : External repositories
* Change built-in plugin updater URL

= 1.0.3 =
* Enhancement: Change tabs system with a a11y library
* Enhancement: Remove thickbox for readme modal
* Enhancement: New debug UX (still in beta)
* Feature: Add count translations updates in adminbar updates link title for screen readers
* Feature: Uninstall process
* BugFix: Infinite update for translations with multiple resources
* Bugfix: Updates log
* Update: .pot file

= 1.0.2 =
* Mask license key in logs
* Code consistency

= 1.0.1 =
* Divi clear template cache
* Fix Product readme tabs styles and typo
* Fix notices conflicts on plugins.php

= 1.0.0 =
* Add plugin self updater
* Multisite support

= 0.0.9 =
* Change Gulp strategy
