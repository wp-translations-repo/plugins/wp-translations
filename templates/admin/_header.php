<?php
/**
 * Partial Admin Header
 *
 * @package     WP_Translations
 * @subpackage  templates/admin
 * @since      1.0.0
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

use WP_Translations\WordPress\Helpers\Helper;
use WP_Translations\WordPress\Helpers\PageHelper;

$currentPage = isset( $_GET['wpt-page'] ) ? $_GET['wpt-page'] : false;
$options     = Helper::getOptions();
$pages       = PageHelper::getPages();

?>

<div class="wrap wpt-page">
  <header class="wpt-header">
    <img src="<?php echo WPTORG_PLUGIN_URL .'assets/img/wpt-logo.svg'; ?>" />
  </header>

  <div class="wpt-nav">
    <div class="wp-filter">
      <ul class="filter-links">

        <?php foreach ( $pages as $slug => $page ) : ?>
          <li <?php if ( 'settings' == $slug ) { echo'class="alignright"'; } ?>>
            <a class="<?php if ( $slug == $currentPage ) { echo'current'; } ?>" href="<?php echo Helper::adminUrl( 'admin.php?page=wp-translations&wpt-page=' . esc_attr( $slug ) ); ?>"><span class="dashicons <?php echo esc_attr( $page['icon'] ); ?>"></span> <span class="wpt-hide-on-sm"><?php echo esc_html( $page['label'] ); ?></span></a>
          </li>
        <?php endforeach; ?>

      </ul>

    </div>

  </div><!-- /end .wpt-nav -->
<h2 class="screen-reader-text" style="margin:0;">WP-Translations</h2>
