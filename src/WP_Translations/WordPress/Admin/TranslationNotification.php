<?php

namespace WP_Translations\WordPress\Admin;

defined( 'ABSPATH' ) or die( 'You don&#8217;t have permission to do this.' );

use WP_Translations\WordPress\Helpers\Helper;
use WP_Translations\WordPress\Helpers\ProductHelper;
use WP_Translations\WordPress\Helpers\ModalHelper;
use WP_Translations\WordPress\Helpers\TranslationHelper;
use WP_Translations\WordPress\Helpers\FeatureHelper;

/**
 * Translations Updater
 *
 * @since 1.0.0
 */

class TranslationNotification {

  public function __construct( $slug, $type, $data, $status ) {
    $this->slug   = $slug;
    $this->type   = $type;
    $this->data   = $data;
    $this->status = $status;
    $this->locale = Helper::getLocale();

    $this->updates = wp_get_translation_updates();
    $this->run();
  }

  public function run() {

    if ( 'plugin' == $this->type && current_user_can( 'update_languages' ) ) {
      remove_action( 'after_plugin_row_' . $this->data, 'wp_plugin_update_row', 10 );
      add_action( 'after_plugin_row_' . $this->data,     array( $this, 'showUpdateNotification' ), 10, 3 );
    }

    if ( 'theme' == $this->type && is_multisite() && current_user_can( 'update_languages' ) ) {
      remove_action( 'after_theme_row_' . $this->slug, 'wp_theme_update_row', 10 );
      add_action( 'after_theme_row_' . $this->slug,     array( $this, 'showUpdateNotification' ), 10, 3 );
    }

  }

  /**
   * show update nofication row -- needed for multisite subsites, because WP won't tell you otherwise!
   *
   * @param string  $file
   * @param array   $plugin
   */
  public function showUpdateNotification( $file, $data, $status ) {

    $options  = Helper::getOptions();

    if ( 'update' == $this->status ) {
      $languages = array();
      foreach ( $this->updates as $update ) {
        if ( $update->slug === $this->slug ) {
          $languages[] = $update->language;
        }
      }

      if ( empty( $languages ) ) {
        return;
      }

      $dataLang = implode( '|', $languages );

      if ( 'plugin' == $this->type ) {
        $status = ( is_plugin_active( $this->data ) ) ? 'active' : 'inactive';
      } else {
        $status = 'inactive';
      }
      $modalArgs = array(
        'button_css_class' => 'wpt-button-link',
        'active_tab'       => 'changelog'
      );
      $count_lp       = count( $languages );
      $message        = esc_html__( 'New translations are available:&nbsp;', 'wp-translations' );
      $update_link    = '<button id="wp-translations-update-' . esc_attr( $this->slug ) . '" class="button-link" type="button" data-type="plugins" data-slug="' . esc_attr( $this->slug ) . '" data-locale="' . $dataLang . '">' . esc_html__( 'Update now', 'wp-translations' ) . '</button>';

      echo '<tr class="plugin-update-tr ' . esc_attr( $status ) . ' wp-translations-update-row" id="' . esc_attr( $this->slug ) . '-update" data-slug="' . esc_attr( $this->slug ) . '" data-plugin="' . esc_attr( $this->data ) . '">';
      echo '<td colspan="3" class="plugin-update colspanchange">';
      echo '<div id="wp-translations-notice-' . esc_attr( $this->slug ) . '" class="wp-translations-update-message wp-translations-notice wp-translations-notice-warning notice-alt"><p>';
      echo $message . implode( ',&nbsp;', $languages ) . '&nbsp;-&nbsp;' . $update_link;
      if ( false !== ProductHelper::isProduct( $this->slug, $this->locale ) ) {
      echo '&nbsp;|&nbsp;';
      ModalHelper::displayReadmeModal( $this->slug, $this->locale, $modalArgs );
      }
      echo '</p></div></td></tr>';

    } elseif( 'notice-info' == $this->status && false ==! FeatureHelper::isEnable( 'premium' ) ) {

      if ( 'en_US' != $this->locale ) {

        if ( 'plugin' == $this->type ) {
          $status = ( is_plugin_active( $this->data ) ) ? 'active' : 'inactive';
        } else {
          $status = 'inactive';
        }

        $modalArgs = array(
          'button_css_class' => 'wpt-button-link',
        );

        $message        = esc_html__( 'A premium translation is available!', 'wp-translations' );
        echo '<tr class="plugin-update-tr ' . esc_attr( $status ) . ' wp-translations-update-row" id="' . esc_attr( $this->slug ) . '-update" data-slug="' . esc_attr( $this->slug ) . '" data-plugin="' . esc_attr( $this->data ) . '">';
        echo '<td colspan="3" class="plugin-update colspanchange">';
        echo '<div id="wp-translations-notice-' . esc_attr( $this->slug ) . '" class="wp-translations-info-message wp-translations-notice wp-translations-notice-info notice-alt"><p>';
        echo $message . '&nbsp;-&nbsp';
        ModalHelper::displayReadmeModal( $this->slug, $this->locale, $modalArgs );

      }
    }
  }

}
