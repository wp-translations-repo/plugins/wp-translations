<?php

namespace WP_Translations\WordPress\Admin;

defined( 'ABSPATH' ) or die( 'You don&#8217;t have permission to do this.' );

use WP_Translations\Models\HooksAdminInterface;
use WP_Translations\WordPress\Helpers\Helper;
use WP_Translations\WordPress\Helpers\ProductHelper;
use WP_Translations\WordPress\Helpers\ModalHelper;
use WP_Translations\WordPress\Admin\TranslationNotification;
/**
 * Translations Setup
 *
 * @since 1.0.0
 */

class TranslationSetup implements HooksAdminInterface {

  public function __construct() {
    $this->options = Helper::getOptions();
    $this->locale  = Helper::getLocale();
  }

  public function hooks() {

    add_action( 'init', array( $this, 'translationsNotifications' ) );
    add_action( 'init', array( $this, 'customTranslations' ), -1 );
    add_action( 'load_textdomain', array( $this, 'logTextdomainCalls' ), 10, 2 );

    if ( false !== (bool) $this->options['settings_ui']['bubble_count'] ) {
      add_filter( 'wp_get_update_data', array( $this, 'translationsUpdateCount' ) );
    }
    if ( ! is_multisite() && current_user_can( 'update_languages' ) ) {
      add_action('admin_footer-themes.php', array( $this, 'themesReadmeModal' ) );
    }

    if ( isset( $this->options['updates']['async_updates'] ) && false === (bool) $this->options['updates']['async_updates'] ) {
      add_filter( 'auto_update_translation',  '__return_false' );
      add_filter( 'async_update_translation', '__return_false' );
    }

  }


  public function translationsNotifications() {

    if ( ! function_exists( 'get_plugins' ) ) {
      require_once ABSPATH . 'wp-admin/includes/plugin.php';
    }

    $translations = wp_list_pluck( wp_get_translation_updates(), 'type', 'slug' );

    $plugins_active = get_plugins();
    $plugins = array();
    foreach ( $plugins_active as $file => $data ) {
      $plugins[ $data['TextDomain'] ] = array(
        'name'       => $data['Name'],
        'textdomain' => $data['TextDomain'],
        'file'       => $file,
      );
      $domains[ $data['TextDomain'] ] = 'plugin';

      if ( false !== ProductHelper::isToPromote( $data['TextDomain'], $this->locale ) ) {
        $status = 'notice-info';
        $notification = new TranslationNotification( $data['TextDomain'], 'plugin', $file, $status );
      }
    }

    $themes = wp_get_themes();
    foreach ( $themes as $key => $theme ) {
      $themes[ $key ] = $theme->get( 'TextDomain' );
      $domains[ $theme->get( 'TextDomain' ) ] = 'theme' ;
    }

    foreach ( $translations as $slug => $type ) {
      $data   = ( 'plugin' == $type ) ? $plugins[ $slug ]['file'] : wp_get_theme( $slug );
      if ( false !== (bool) $this->options['settings_ui'][ 'plugins_updates'] || false !== (bool) $this->options['settings_ui'][ 'themes_updates'] )  {
        $notification = new TranslationNotification( $slug, $type, $data, 'update' );
      }
    }

  }

  public function logTextdomainCalls( $domain, $mofile ) {

    global $wp_version;
    $translationSettings = ( false !== get_site_option( 'wpt_translations' ) ) ? get_site_option( 'wpt_translations' ) : array();

    if ( is_readable( $mofile ) && 'en_US' != $this->locale ) {
      $translationSettings[ $domain ][ $this->locale ]['loaded_mo_file'] = $mofile;
    } else {
      unset( $translationSettings[ $domain ][ $this->locale ]['loaded_mo_file'] );
    }

    update_site_option( 'wpt_translations', $translationSettings );

  }

  public function customTranslations() {

    $locale = Helper::getLocale();
    $translationSettings = ( false !== get_site_option( 'wpt_translations' ) ) ? get_site_option( 'wpt_translations' ) : array();
    $coreDomains = array( 'default', 'admin', 'admin-network', 'continents-cities' );
    $adminTranslations = array();

    foreach ( $translationSettings as $domain => $settings ) {
      if ( isset( $settings[ $locale ]['custom_translation']['path'] ) ) {
        if ( ! in_array( $domain, $coreDomains ) ) {
          unload_textdomain( $domain );
          load_textdomain( $domain, $settings[ $locale ]['custom_translation']['path'] );
        } else {
          $adminTranslations[] = $settings[ $locale ]['custom_translation']['path'];
        }
      }
    }

    if ( ! empty( $adminTranslations ) ) {
      unload_textdomain( 'default' );
      foreach ( $adminTranslations as $customFile ) {
        load_textdomain( 'default', $customFile );
      }
    }

  }

  public function translationsUpdateCount( $update_data ) {

    $update_data['counts']['translations'] = ( 0 < count( wp_get_translation_updates() ) ) ? count( wp_get_translation_updates() ) : '';
    if ( 1 < count( wp_get_translation_updates() ) ) {
      $update_data['counts']['total'] = ( count( wp_get_translation_updates() ) + $update_data['counts']['total'] ) - 1;
    }

    $titles = array();
    if ( $update_data['counts']['wordpress'] ) {
      /* translators: 1: Number of updates available to WordPress */
      $titles['wordpress'] = sprintf( __( '%d WordPress Update', 'wp-translations' ), $update_data['counts']['wordpress'] );
    }
    if ( $update_data['counts']['plugins'] ) {
      /* translators: 1: Number of updates available to plugins */
      $titles['plugins'] = sprintf( _n( '%d Plugin Update', '%d Plugin Updates', $update_data['counts']['plugins'], 'wp-translations' ), $update_data['counts']['plugins'] );
    }
    if ( $update_data['counts']['themes'] ) {
      /* translators: 1: Number of updates available to themes */
      $titles['themes'] = sprintf( _n( '%d Theme Update', '%d Theme Updates', $update_data['counts']['themes'], 'wp-translations' ), $update_data['counts']['themes'] );
    }
    if ( $update_data['counts']['translations'] ) {
      /* translators: 1: Number of updates available to translations */
      $titles['translations'] = sprintf( _n( '%d Translation Update', '%d Translation Updates', $update_data['counts']['translations'], 'wp-translations' ), $update_data['counts']['translations'] );
    }
    $update_data['title'] = $titles ? esc_attr( implode( ', ', $titles ) ) : '';

    return $update_data;
  }

  public function themesReadmeModal() {
    $currentLocale = Helper::getLocale();
    $products  = ProductHelper::localProducts();
    if ( 'en_US' != $currentLocale && ! empty( $products[ $currentLocale ] ) ) {
      foreach ( $products[ $currentLocale ] as $product ) {
        if ( 'theme' == $product['type'] && false !== ProductHelper::isToPromote( $product['slug'], $currentLocale ) ) {
          $modalArgs = array(
            'show_button'      => false,
            'active_tab'       => 'description'
          );
          ModalHelper::displayReadmeModal( $product['slug'], $currentLocale, $modalArgs );
        }
      }
    }

  }

}
