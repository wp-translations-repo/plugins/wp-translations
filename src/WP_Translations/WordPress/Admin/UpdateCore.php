<?php

namespace WP_Translations\WordPress\Admin;

defined( 'ABSPATH' ) or die( 'You don&#8217;t have permission to do this.' );

use WP_Translations\Models\HooksAdminInterface;
use WP_Translations\WordPress\Helpers\Helper;
use WP_Translations\WordPress\Helpers\ProductHelper;
use WP_Translations\WordPress\Helpers\TranslationHelper;
use WP_Translations\WordPress\Helpers\ModalHelper;
use WP_Translations\WordPress\Helpers\RepositoryHelper;

/**
 * Update Translations list in updagre-core.php
 *
 * @since 1.0.0
 */

class UpdateCore implements HooksAdminInterface {

  /**
   * @see WP_Translations\Models\HooksInterface
   */
  public function hooks() {
    $options = Helper::getOptions();
    if ( false !== (bool) $options['settings_ui']['core_updates'] ) {
      add_action( 'core_upgrade_preamble', array( $this, 'listTranslationsUpdate' ) );
    }
  }

  public function listTranslationsUpdate() {

    global $wp_version;

    $updates          = wp_get_translation_updates();
    $count_updates_lp = count( $updates );
    $products         = ProductHelper::localProducts();
    $settings         = get_site_option( 'wpt_translations' );

    if ( ! empty( $updates ) ) :
    ?>
    <table class="wpt-settings-table row-border" id="update-translations-table">
      <thead>
      <tr>
        <th class="no-sort check-column"><input type="checkbox" id="translations-select-all" /></th>
        <th><?php esc_html_e( 'Text Domain', 'wp-translations' ); ?></th>
        <th class="wpt-hide-on-xs"><?php esc_html_e( 'Type', 'wp-translations' ); ?></th>
        <th><?php esc_html_e( 'Languages', 'wp-translations' ); ?></th>
        <th class="wpt-hide-on-lg"><?php esc_html_e( 'Current Version', 'wp-translations' ); ?></th>
        <th class="wpt-hide-on-md"><?php esc_html_e( 'Available Version', 'wp-translations' ); ?></th>
        <th class="wpt-hide-on-md"><?php esc_html_e( 'Source', 'wp-translations' ); ?></th>
        <th class="no-sort column-actions"><?php esc_html_e( 'Actions', 'wp-translations' ); ?></th>
      </tr>
      </thead>
      <tbody class="translations">
        <?php

        foreach ( $updates as $update ) :

          $checkbox_id = 'checkbox_' . md5( 'lp_' . $update->slug );
          switch ( $update->type ) {
            case 'core':
              /* translators: Icon type. */
              $type = '<span class="dashicons dashicons-wordpress"></span><span class="screen-reader-text">' . esc_html__( 'Core', 'wp-translations' ) . '</span>';
              $data_type = $update->type;
              $slug = TranslationHelper::rewriteTextdomain( $update->slug );
              $version = $wp_version;
              break;

            case 'plugin':
              /* translators: Icon type. */
              $type = '<span class="dashicons dashicons-admin-plugins"></span><span class="screen-reader-text">' . esc_html__( 'Plugins', 'wp-translations' ) . '</span>';
              $data_type = $update->type . 's';
              $slug = TranslationHelper::rewriteTextdomain( $update->slug );
              $plugins_active = get_plugins();
              foreach ( $plugins_active as $data ) {
                $plugins[ $data['TextDomain'] ] = $data['Version'];
              }
              $version = $plugins[ $update->slug ];
              break;

            case 'theme':
              /* translators: Icon type. */
              $type = '<span class="dashicons dashicons-admin-appearance"></span><span class="screen-reader-text">' . esc_html__( 'Themes', 'wp-translations' ) . '</span>';
              $data_type = $update->type . 's';
              $slug = TranslationHelper::rewriteTextdomain( $update->slug );
              $themes = wp_get_themes();
              foreach ( $themes as $key => $theme ) {
                $themes[ $key ] = $theme->get( 'Version' );
              }
              $version = $themes[ $slug ];
              break;
          }

          $translations     = wp_get_installed_translations( $data_type );
          $currentVersion   = isset( $translations[ $slug ][ $update->language ] ) ? $version . ' rev(' .strtotime( $translations[ $slug ][ $update->language ]['PO-Revision-Date'] ) . ')' : esc_html__( 'No translations found', 'wp-translations' );
          $availableVersion = ( isset( $update->po_version ) ) ? $update->version . ' rev(' . strtotime( $update->po_version ) . ')' : $update->version . ' rev(' . strtotime( $update->updated ) . ')';
          ?>

          <tr id="wpt-license-row-<?php echo $update->language; ?>-<?php echo $update->slug; ?>" class="wpt-license-row">
            <td>
              <input type="checkbox" class="wpt-translations-checked" name="wp-translations-checked[]" id="<?php echo esc_attr( $checkbox_id ); ?>" data-locale="<?php echo esc_attr( $update->language ); ?>" data-type="<?php echo esc_attr( $update->type ); ?>" value="<?php echo esc_attr( $update->slug ); ?>" />
              <label for="<?php echo esc_attr( $checkbox_id ); ?>" class="screen-reader-text"></label>
            </td>
            <td class="plugin-title">
              <strong><?php echo esc_attr( $update->slug ); ?></strong>
            </td>
            <td class="wpt-hide-on-xs"><?php echo $type; ?></td>
            <td><?php echo $update->language; ?></td>
            <td class="wpt-hide-on-lg"><?php echo $currentVersion; ?></td>
            <td class="wpt-hide-on-md"><?php echo $availableVersion; ?></td>
            <td class="wpt-hide-on-md">
              <?php
                $update->repo = ( ! isset( $update->repo ) ) ? 'wordpress' : $update->repo;
                echo RepositoryHelper::getRepoName( $update->repo );
              ?>
            </td>
            <td class="column-actions">
              <?php
                $slug = TranslationHelper::rewriteTextdomain( $update->slug );
                if ( ProductHelper::isProduct( TranslationHelper::sanitizeTextdomain( $slug ), $update->language ) ) :

                $modalArgs = array(
                  'button_css_class' => 'wpt-button',
                  'button_text'      => __( 'Changelog', 'wp-translations' ),
                  'button_icon'			 => 'dashicons-backup',
                  'active_tab'       => 'changelog'
                );
                ModalHelper::displayReadmeModal( TranslationHelper::sanitizeTextdomain( $slug ), $update->language, $modalArgs );
              endif; ?>

              <button id="wp-translations-update-<?php echo esc_attr( $slug ); ?>-<?php echo $update->language; ?>" class="wpt-button wpt-button-update wp-translations-to-update" type="button" data-type="<?php echo esc_attr( $data_type ); ?>" data-locale="<?php echo $update->language; ?>" data-slug="<?php echo esc_attr( $update->slug ); ?>"><span class="dashicons dashicons-update"></span> <span class="wpt-hide-on-md"><?php esc_html_e( 'Update now', 'wp-translations' ); ?></span></button><div id="wp-translations-update-result-<?php echo esc_attr( $update->slug ); ?>-<?php echo $update->language; ?>" class="screen-reader-text"></div>
            </td>
          </tr>

        <?php endforeach; ?>

      </tbody>
      <tfoot>
      </tfoot>
    </table>
  <?php endif;
  }

}
