<?php

namespace WP_Translations\WordPress\Admin;

defined( 'ABSPATH' ) or die( 'You don&#8217;t have permission to do this.' );

use WP_Translations\Models\HooksAdminInterface;
use WP_Translations\WordPress\Helpers\Helper;
use WP_Translations\WordPress\Helpers\ProductHelper;
use WP_Translations\WordPress\Helpers\PageHelper;
use WP_Translations\WordPress\Helpers\TranslationHelper;
use WP_Translations\WordPress\Admin\TranslationNotification;

/**
 * Enqueue Styles and Scripts
 *
 * @since 1.0.0
 */

class EnqueueAssets implements HooksAdminInterface {

  /**
   * @see WP_Translations\Models\HooksInterface
   */
  public function hooks() {
    add_action( 'admin_enqueue_scripts', array( $this, 'enqueueStyles' ) );
    add_action( 'admin_enqueue_scripts', array( $this, 'enqueueScripts' ) );
    add_action( 'admin_enqueue_scripts', array( $this, 'inlineStyles' ) );
  }

  public function enqueueStyles() {
    $options = Helper::getOptions();
    $screen  = get_current_screen();
    $css_ext = ( defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ) ? '.css' : '.min.css';

    wp_register_style(
      'wpt-admin-styles',
      WPTORG_PLUGIN_URL . 'assets/css/wpt-styles' . $css_ext,
      array(),
      WPTORG_VERSION
    );

    wp_register_style(
      'wpt-datatables',
      WPTORG_PLUGIN_URL . 'assets/css/jquery.dataTables' . $css_ext,
      array(),
      '1.10.16'
    );

    if ( in_array( $screen->base, Helper::assetsScreen() ) ) {
      wp_enqueue_style( 'wpt-admin-styles' );
      wp_enqueue_style( 'wpt-datatables' );
    }

    $updateCoreScreen = is_multisite() ? 'update-core-network' : 'update-core';

    if ( isset( $screen ) && $updateCoreScreen === $screen->id && false !== (bool) $options['settings_ui']['core_updates'] ) {
      wp_enqueue_style( 'wpt-admin-styles' );
      wp_enqueue_style( 'wpt-datatables' );
    }

  }

  public function enqueueScripts() {

    $screen         = get_current_screen();
    $options        = Helper::getOptions();
    $products       = ProductHelper::localProducts();
    $storesData     = ProductHelper::STORES;
    $js_ext         = ( defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ) ? '.js' : '.min.js';
    $js_dir         = ( defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ) ? '' : 'min/';
    $currentLocale  = Helper::getLocale();

    $themesTranslations = array();
    $themesUpdates = wp_get_translation_updates();

    $datatableTranslations = array(
      'sEmptyTable' 		=> esc_html__( 'No data available', 'wp-translations' ),
      'sInfo' 					=> esc_html_x( 'Showing _START_ to _END_ of _TOTAL_ entries', 'Please don\'t translate: _START_ _END_ _TOTAL_', 'wp-translations' ),
      'sInfoEmpty' 			=> esc_html__( 'Showing 0 to 0 of 0 entries', 'wp-translations' ),
      'sInfoFiltered' 	=> esc_html_x( 'filtered from _MAX_ total entries', 'Please don\'t translate: _MAX_', 'wp-translations' ),
      'sLengthMenu' 		=> esc_html_x( 'Show _MENU_ entries', 'Please don\'t translate: _MENU_', 'wp-translations' ),
      'sLoadingRecords' => esc_html__( 'Processing...', 'wp-translations' ),
      'sSearch' 				=> esc_html__( 'Search:', 'wp-translations' ),
      'sZeroRecords' 		=> esc_html__( 'No matching records found', 'wp-translations' ),
      'sFirst' 					=> esc_html__( 'First', 'wp-translations' ),
      'sLast' 					=> esc_html__( 'Last', 'wp-translations' ),
      'sNext' 					=> esc_html__( 'Next', 'wp-translations' ),
      'sPrevious' 			=> esc_html__( 'Previous', 'wp-translations' ),
      'sSortAscending' 	=> esc_html__( ': activate to sort column ascending', 'wp-translations' ),
      'sSortDescending' => esc_html__( ': activate to sort column descending', 'wp-translations' ),
    );

    foreach ( $themesUpdates as $theme ) {
      if( 'theme' == $theme->type ) {
        $themesTranslations[ $theme->slug ]['updates'][] = $theme->language;
      }
    }

    $allThemes         = wp_get_themes();
    $themesProducts    = array();
    foreach ( $allThemes as $theme ) {

      $themeSlug = ( ! empty( $theme->get( 'TextDomain' ) ) ) ? $theme->get( 'TextDomain' ) : TranslationHelper::sanitizeTextdomain( $theme->get( 'Name' ) );
      if ( false !== ProductHelper::isToPromote( $themeSlug, $currentLocale ) ) {
        $themesProducts[ $themeSlug ] = $themeSlug;
      }

    }

    $wpt_license_data = array(
      'ajaxurl'           => admin_url( 'admin-ajax.php' ),
      'nonce'             => wp_create_nonce( 'wpt-license-nonce' ),
      'update_nonce'      => wp_create_nonce( 'wpt-update-nonce' ),
      'repository_nonce'  => wp_create_nonce( 'wpt-repository-nonce' ),
      'translation_nonce' => wp_create_nonce( 'wpt-translations-nonce' ),
      'stores'            => $storesData,
      'products'          => ProductHelper::localProducts(),
      'dropdown_roles'    => Helper::dropdownRoles(),
      'dropdown_caps'     => Helper::dropdownCaps(),
      'i18n'              => array(
        'btn'       => array(
          'save'            => esc_html__( 'Save License', 'wp-translations' ),
          'deleted'         => esc_html__( 'Delete License', 'wp-translations' ),
          'activate'        => esc_html__( 'Activate', 'wp-translations' ),
          'deactivate'      => esc_html__( 'Deactivate', 'wp-translations' ),
          'renew'           => esc_html__( 'Renew', 'wp-translations' ),
          'upgrade'         => esc_html__( 'Upgrade', 'wp-translations' ),
          'download'        => esc_html__( 'Install translations', 'wp-translations' ),
          'done'            => esc_html__( 'Activated and installed', 'wp-translations' ),
          'add_new_cap'     => esc_html__( 'Add a new rule', 'wp-translations' ),
        ),
        'status'    => array(
          'expired'         => esc_html__( 'License expired', 'wp-translations' ),
          'activated'       => esc_html__( 'License activated', 'wp-translations'),
          'can_activated'   => esc_html__( 'License can be activated', 'wp-translations'),
          'deactivated'     => esc_html__( 'License deactivated', 'wp-translations' ),
        ),
        'messages'  => array(
          'check_license'   => esc_html__( 'Check license status', 'wp-translations' ),
          'download_lp'     => esc_html__( 'Downloading and installing your language pack', 'wp-translations' ),
          'check_update'    => esc_html__( 'Check for updates', 'wp-translations' ),
          'clear_cache'     => esc_html__( 'Clearing cache', 'wp-translations' ),
          'lp_installed'    => esc_html__( 'Language pack installed!', 'wp-translations' ),
          'deleting'        => esc_html__( 'Deleting license', 'wp-translations' ),
          'saving'          => esc_html__( 'Saving license', 'wp-translations' ),
          'deactivating'    => esc_html__( 'Deactivating license', 'wp-translations' ),
        ),
        'repo'      => array(
          'delete'          => esc_html__( 'Delete repository', 'wp-translations' ),
          'deleting'        => esc_html__( 'Deleting repository', 'wp-translations' ),
          'saving'          => esc_html__( 'Saving repository', 'wp-translations' ),
          'deactivating'    => esc_html__( 'Deactivating repository', 'wp-translations' ),
          'activating'      => esc_html__( 'Activating repository', 'wp-translations' ),
          'edit'            => esc_html__( 'Edit', 'wp-translations' ),
          'close_modal'     => esc_html__( 'Close modal', 'wp-translations' ),
          'add_title'       => esc_html__( 'Add a Repository', 'wp-translations' ),
          'edit_title'      => esc_html__( 'Edit Repository', 'wp-translations' ),
        ),
        'translation' => array(
          'saving'          => esc_html__( 'Saving settings', 'wp-translations' ),
          'all'             => esc_html__( 'All', 'wp-translations' ),
          'delete_file'     => esc_html__( 'Delete custom file', 'wp-translations' ),
          'deleting_file'   => esc_html__( 'Deleting custom file', 'wp-translations' ),
          'deleted_file'    => esc_html__( 'Custom file deleted', 'wp-translations' ),
        ),
        'locale' => array(
          'deleting'        => esc_html__( 'Deleting translations files', 'wp-translations' ),
          'installing'      => esc_html__( 'Installing translations files', 'wp-translations' ),
          'deleted'         => esc_html__( 'Translations files deleted', 'wp-translations' ),
          'installed'       => esc_html__( 'Translations files installed', 'wp-translations' ),
          'error'           => esc_html__( 'Something went wrong.', 'wp-translations' ),
        ),
        'datatable'         => $datatableTranslations,
      ),
    );

    $wpt_update_core = array(
      'ajaxurl'             => admin_url( 'admin-ajax.php' ),
      'nonce'               => wp_create_nonce( 'wpt-update-nonce' ),
      'updating_message'    => esc_html__( 'Updating translations', 'wp-translations' ),
      'updated_message'     => esc_html__( 'Translations updated', 'wp-translations' ),
      'all_updated_message' => esc_html__( 'The translations are up to date.', 'wp-translations' ),
      'i18n' => array(
        'messages'  => array(
          'download_lp'     => esc_html__( 'Downloading and installing your language pack', 'wp-translations' ),
          'check_update'    => esc_html__( 'Check for updates', 'wp-translations' ),
          'clear_cache'     => esc_html__( 'Clearing cache', 'wp-translations' ),
          'lp_installed'    => esc_html__( 'Language pack installed!', 'wp-translations' ),
        ),
        'datatable'         => $datatableTranslations,
      ),
    );

    $wpt_update_ajax = array(
      'ajaxurl'             => admin_url( 'admin-ajax.php' ),
      'nonce'               => wp_create_nonce( 'wpt-update-nonce' ),
      'plugins_updates'     => (bool) $options['settings_ui']['plugins_updates'],
      'themes_updates'      => (bool) $options['settings_ui']['themes_updates'],
      'themes_translations' => $themesTranslations,
      'themes_products'     => $themesProducts,
      'update_message'      => esc_html__( 'New translations are available:&nbsp;', 'wp-translations' ),
      'update_button'       => esc_html__( 'Update now' , 'wp-translations' ),
      'promote_message'     => esc_html__( 'A premium translation is available!', 'wp-translations' ),
      'view_details'        => esc_html__( 'View details', 'wp-translations' ),
    );

    wp_register_script(
      'wpt-admin-scripts',
      WPTORG_PLUGIN_URL . 'assets/js/' . $js_dir . 'wpt-scripts' . $js_ext,
      array( 'jquery' ),
      WPTORG_VERSION,
      true
    );

    wp_register_script(
      'wpt-tabs',
      WPTORG_PLUGIN_URL . 'assets/js/' . $js_dir . 'jquery-accessible-tabs-aria' . $js_ext,
      array( 'jquery' ),
      '1.6.1',
      true
    );

    wp_register_script(
      'wpt-modal',
      WPTORG_PLUGIN_URL . 'assets/js/' . $js_dir . 'jquery-accessible-modal-window-aria' . $js_ext,
      array( 'jquery' ),
      '1.9.2',
      true
    );

    wp_register_script(
      'wpt-tooltip',
      WPTORG_PLUGIN_URL . 'assets/js/' . $js_dir . 'jquery-accessible-simple-tooltip-aria' . $js_ext,
      array( 'jquery' ),
      '2.2.0',
      true
    );

    wp_register_script(
      'wpt-hide-show',
      WPTORG_PLUGIN_URL . 'assets/js/' . $js_dir . 'jquery-accessible-hide-show-aria' . $js_ext,
      array( 'jquery' ),
      '1.8.0',
      true
    );

    wp_register_script(
      'wpt-update-core',
      WPTORG_PLUGIN_URL . 'assets/js/' . $js_dir . 'wpt-update-core' . $js_ext,
      array( 'jquery' ),
      WPTORG_VERSION,
      true
    );

    wp_register_script(
      'wpt-notifications',
      WPTORG_PLUGIN_URL . 'assets/js/' . $js_dir . 'wpt-notifications' . $js_ext,
      array( 'jquery' ),
      WPTORG_VERSION,
      true
    );

    wp_register_script(
      'wpt-datatables',
      WPTORG_PLUGIN_URL . 'assets/js/' . $js_dir . 'jquery.dataTables' . $js_ext,
      array( 'jquery' ),
      '1.10.16',
      true
    );

    if ( in_array( $screen->base, Helper::assetsScreen() ) ) {
      wp_enqueue_media();
      wp_enqueue_script( 'wpt-tabs' );
      wp_enqueue_script( 'wpt-modal' );
      wp_enqueue_script( 'wpt-tooltip' );
      wp_enqueue_script( 'wpt-hide-show' );
      wp_enqueue_script( 'wpt-datatables' );
      wp_enqueue_media();
      wp_enqueue_script( 'wpt-admin-scripts' );
      wp_localize_script( 'wpt-admin-scripts', 'wpt_license_ajax', $wpt_license_data );
    }

    $updateCoreScreen = is_multisite() ? 'update-core-network' : 'update-core';

    if ( isset( $screen ) && $updateCoreScreen === $screen->id && false !== (bool) $options['settings_ui']['core_updates'] ) {
      wp_enqueue_script( 'wpt-tabs' );
      wp_enqueue_script( 'wpt-modal' );
      wp_enqueue_script( 'wpt-update-core' );
      wp_localize_script( 'wpt-update-core', 'wpt_update_core', $wpt_update_core );
      wp_enqueue_script( 'wpt-datatables' );
    }

    if ( 'plugins' == $screen->id && false !== (bool) $options['settings_ui']['plugins_updates'] || 'themes' == $screen->id && false !== (bool) $options['settings_ui']['themes_updates'] ) {
      wp_enqueue_script( 'wpt-notifications' );
      wp_localize_script( 'wpt-notifications', 'wpt_update_ajax', $wpt_update_ajax );
    }

  }

  function inlineStyles() {
    wp_enqueue_style( 'admin-menu' );

    $custom_css = "
      #adminmenu #toplevel_page_wp-translations-pro div.wp-menu-image img {
          padding-top: 5px;
          max-width: 24px;
      }";
    wp_add_inline_style( 'admin-menu', $custom_css );
  }

}
