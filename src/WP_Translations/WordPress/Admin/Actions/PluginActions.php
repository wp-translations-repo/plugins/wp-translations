<?php

namespace WP_Translations\WordPress\Admin\Actions;

defined( 'ABSPATH' ) or die( 'You don&#8217;t have permission to do this.' );

use WP_Translations\Models\HooksAdminInterface;
use WP_Translations\Models\ActivationInterface;
use WP_Translations\WordPress\Helpers\FileHelper;
use WP_Translations\WordPress\Helpers\RoutineHelper;

/**
 * PluginUpdater Actions
 *
 * @since 1.0.0
 */

class PluginActions implements HooksAdminInterface, ActivationInterface {


  public function hooks() {

  }
  /**
   * Set Default Options
   */
  public function activation() {

    $defaults = array(
      'features' => array(
        'repositories' => 1,
        'premium'       => 1,
        'capabilities'  => '',
        'performance'   => '',
      ),
      'settings_ui' => array(
        'core_updates'    => 1,
        'plugins_updates' => '',
        'themes_updates'  => '',
        'bubble_count'    => 1,
        'page_position'   => 'menu',
      ),
      'updates' => array(
        'async-updates'   => 1,
      ),
      'languages' => array(
        'front_user_locale' => '',
      ),
      'advanced' => array(
        'debug_mode'           => '',
      ),
      'licenses'        => array(),
    );
    add_site_option( 'wpt_settings', $defaults );
    add_site_option( 'wpt_logs', array(
      'update'  => array(),
      'license' => array()
    ));
    add_site_option( 'wpt_translations', array() );

    if ( ! is_dir( WPTORG_CONTENT_DIR ) ) {
      FileHelper::makeDir( WP_CONTENT_DIR . '/wp-translations' );
    }

    if ( ! is_dir( WPTORG_CONTENT_DIR . '/embeded' ) ) {
      FileHelper::makeDir( WPTORG_CONTENT_DIR . '/embeded' );
    }

    RoutineHelper::triggerUpgrades();

  }

}
