<?php

namespace WP_Translations\WordPress\Admin\Actions;

defined( 'ABSPATH' ) or die( 'You don&#8217;t have permission to do this.' );

use WP_Translations\Models\HooksAdminInterface;
use WP_Translations\WordPress\Helpers\Helper;
use WP_Translations\WordPress\Helpers\PageHelper;
use WP_Translations\WordPress\Admin\Page\PageSettings;
use WP_Translations\WordPress\Helpers\ProductHelper;
use WP_Translations\WordPress\Helpers\TranslationHelper;

/**
 * settings Actions
 *
 * @since 1.0.0
 */

class SettingsActions implements HooksAdminInterface {

  /**
   * @see WP_Translations\Models\HooksInterface
   */
  public function hooks() {
    add_action( 'admin_init',           array( $this, 'processActions' ) );
    add_action( 'wpt_saveSettings',     array( $this, 'wpt_saveSettings' ) );
    add_action( 'wpt_switchLanguage',   array( $this, 'wpt_switchLanguage' ) );
    add_action( 'wpt_moCacheToggle',    array( $this, 'wpt_moCacheToggle' ) );
  }

  public function processActions() {

    if ( isset( $_POST['wpt-action'] ) ) {
      do_action( 'wpt_' . $_POST['wpt-action'], $_POST );
    }

    if ( isset( $_GET['wpt-action'] ) ) {
      do_action( 'wpt_' . $_GET['wpt-action'], $_GET );
    }

  }

  /**
   * Switch Language in translations list table
   */
  public function wpt_switchLanguage( $data ) {

    if ( ! isset( $data['wpt-switch-locale-nonce'] ) || ! wp_verify_nonce( $data['wpt-switch-locale-nonce'], 'wpt_switch_locale_nonce' ) ) {
      wp_die( esc_html__( 'Trying to cheat or something?', 'wp-translations' ), esc_html__( 'Error', 'wp-translations' ), array( 'response' => 403 ) );
    }

    $options = Helper::getOptions();

    if ( isset( $data['wpt-locale-switcher'] ) ) {
      $message = '&locale=' . esc_attr( $data['wpt-locale-switcher'] );
      $pageRedirect = ( 'menu' == $options['settings_ui']['page_position'] ) ? 'admin.php' : 'options-general.php';
      wp_redirect( admin_url( $pageRedirect . '?page=wp-translations&wpt-page=translations' . $message ) );
    }

  }

  /**
   * Saves settings
   *
   * @since 1.0
   * @param array $data settings post data
   * @return void
   */
  public function wpt_saveSettings( $data ) {

    if ( ! isset( $data['wpt-settings-nonce'] ) || ! wp_verify_nonce( $data['wpt-settings-nonce'], 'wpt_settings_nonce' ) ) {
      wp_die( esc_html__( 'Trying to cheat or something?', 'wp-translations' ), esc_html__( 'Error', 'wp-translations' ), array( 'response' => 403 ) );
    }

    $options      = Helper::getOptions();
    $fieldsGroup  = PageSettings::setFields();

    if ( isset( $data['wpt_settings']['advanced']['force_products'] ) ) {

      $stores = ProductHelper::STORES;

      foreach ( $stores as $locale => $store ) {
        $products = ProductHelper::localProducts();
        foreach ( $products as $locale => $product ) {
          foreach( $product as $slug => $translation ) {
            delete_site_transient( 'wpt_readme_' . $slug . '_' . $locale );
          }
        }
        delete_site_transient( 'wpt_edd_products_' . $locale );
      }

      $message = '&wpt-message=products_reloaded';

    } elseif ( isset( $data['wpt_settings']['advanced']['force_translations'] ) ) {

      Helper::clearUpdateCache( 'all' );
      $message = '&wpt-message=check_translations_updates';

    } elseif ( isset( $data['wpt_settings']['advanced']['reset_translations'] ) ) {

      delete_site_option( 'wpt_translations' );
      $message = '&wpt-message=reset_translations';

    } elseif ( isset( $data['wpt_settings']['advanced']['purge_logs'] ) ) {

      delete_site_option( 'wpt_logs' );
      $message = '&wpt-message=purge_logs';

    } else {

      foreach ( $fieldsGroup['settings'] as $group => $fields ) {
        foreach ( $fields as $name => $field ) {
          if ( 'button' != $field['type'] ) {
            $options[ sanitize_title( $group ) ][ sanitize_title( $name ) ] = isset( $data['wpt_settings'][ sanitize_title( $group ) ][ sanitize_title( $name )  ] ) ? $data['wpt_settings'][ sanitize_title( $group ) ][ sanitize_title( $name )  ] : '';
          }
        }
      }

      Helper::updateOptions( $options );
      $message = '&wpt-message=settings_updated';
    }

    $pageRedirect = ( 'menu' == $options['settings_ui']['page_position'] ) ? 'admin.php' : 'options-general.php';
    wp_redirect( admin_url( $pageRedirect . '?page=wp-translations&wpt-page=settings' . $message ) );
  }

}
