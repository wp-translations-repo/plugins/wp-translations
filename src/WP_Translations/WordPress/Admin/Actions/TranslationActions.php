<?php

namespace WP_Translations\WordPress\Admin\Actions;

defined( 'ABSPATH' ) or die( 'You don&#8217;t have permission to do this.' );

use WP_Translations\Models\HooksAdminInterface;
use WP_Translations\WordPress\Helpers\Helper;
use WP_Translations\WordPress\Helpers\RepositoryHelper;

use Gettext\Translations;
use Gettext\Generators;

/**
 * Translation Actions
 *
 * @since 1.0.0
 */

class TranslationActions implements HooksAdminInterface {

  protected $options;
  protected $global;

  public function __construct() {
    $this->options = ( get_site_option( 'wpt_translations' ) ) ? get_site_option( 'wpt_translations' ) : array();
    $this->global  = ( get_site_option( 'wpt_global_translations' ) ) ? get_site_option( 'wpt_global_translations' ) : array();
  }

  /**
   * @see WP_Translations\Models\HooksInterface
   */
  public function hooks() {
    add_action( 'wp_ajax_saveTranslationSettings', array( $this, 'saveTranslationSettings' ) );
    add_action( 'wp_ajax_deleteCustomFile',        array( $this, 'deleteCustomFile' ) );
    add_action( 'wp_ajax_getTranslationSettings',  array( $this, 'getTranslationSettings' ) );
  }

  public function saveTranslationSettings() {

    if ( ! wp_verify_nonce( $_POST['nonce'], 'wpt-translations-nonce' ) ) {
      wp_die( esc_html__( 'You don&#8217;t have permission to do this.', 'wp-translations' ) );
    }

    $errors     = array();
    $settings  = array();

    if ( false !== (bool) $_POST['use_custom'] ) {

      if ( isset( $_FILES ) && ! empty ( $_FILES ) )  {

        if ( ! function_exists( 'wp_handle_upload' ) ) {
          require_once( ABSPATH . 'wp-admin/includes/file.php' );
        }

        add_filter( 'upload_dir', array( $this, 'customUploadDir' ) );
        $customFile = wp_handle_upload(
          $_FILES['file'],
          array(
            'test_form' => false,
            'mimes' => array(
              'mo' => 'application/octet-stream',
              'po' => 'application/octet-stream'
            ),
          )
        );
        remove_filter( 'upload_dir', array( $this, 'customUploadDir' ) );


        if ( ! empty( $customFile['error'] ) ) {
          $errors[] = array(
            'field'   => 'custom_translation',
            'message' => $customFile['error'],
          );
        } else {
          $name = $_FILES['file']['name'];
          list( $textdomain, $ext ) = explode( ".", $name );

          if ( 'po' == $ext ) {
            $moPath = str_replace( '.po', '.mo', $customFile['file'] );
            $translations = Translations::fromPoFile( $customFile['file'] );
            $translations->toMoFile( $moPath );
          }

          $settings['custom_translation'] = array(
            'filename' => $name,
            'path' => ( 'po' == $ext ) ? $moPath : $customFile['file'],
          );
        }

      } else {
        $errors[] = array(
          'field'   => 'translation-custom_translation',
          'message' => esc_html__( 'Please select a file', 'wp-translations' ),
        );
      }

    }

    if ( empty( $errors ) ) {

      $slug          = $_POST['slug'];
      $locale        = $_POST['locale'];

      $settings['lock_translation'] = absint( $_POST['lock_translation'] );
      $settings['repo_priority']    = sanitize_title( $_POST['repo_priority'] );
      $settings['use_custom']       = absint( $_POST['use_custom'] );

      $this->options[ $slug ][ $locale ] = (array) $settings;
      update_site_option( 'wpt_translations', $this->options );

      $newRepo = RepositoryHelper::getRepoName( $_POST['repo_priority'] );
      $data = array(
        'message' => esc_html__( 'Settings saved!', 'wp-translations' ),
        'repo'    => $newRepo,
        'settings' => $settings,
      );
      wp_send_json_success( $data );

    } else {
      $data = array(
        'message' => esc_html__( 'Something went wrong.', 'wp-translations'),
        'errors'  => $errors
      );
      wp_send_json_error( $data );
    }

    die();
  }

  public function deleteCustomFile() {

    if ( ! wp_verify_nonce( $_POST['nonce'], 'wpt-translations-nonce' ) ) {
      wp_die( esc_html__( 'You don&#8217;t have permission to do this.', 'wp-translations' ) );
    }

    $slug     = $_POST['slug'];
    $locale   = $_POST['locale'];

    $translationsSettings = get_site_option ('wpt_translations' );
    $moPath = $translationsSettings[ $slug ][ $locale ]['custom_translation']['path'];
    $poPath = str_replace( '.mo', '.po', $moPath );

    if ( @is_readable( $moPath ) ) {
      unlink( $moPath );
    }
    if ( @is_readable( $poPath ) ) {
      unlink( $poPath );
    }
    unset( $translationsSettings[ $slug ][ $locale ]['custom_translation'] );

    update_site_option( 'wpt_translations', $translationsSettings );

    $data = array(
      'html' => '<input id="translation-custom_translation-' . esc_attr( $slug ) . '-' . esc_attr( $locale ) . '" name="wpt_translations[' . esc_attr( $slug ) . '][' . esc_attr( $locale ) . '][custom-translation]" type="file">'
    );
    wp_send_json_success( $data );

  }

  public function getTranslationSettings() {

    if ( ! wp_verify_nonce( $_POST['nonce'], 'wpt-translations-nonce' ) ) {
      wp_die( esc_html__( 'You don&#8217;t have permission to do this.', 'wp-translations' ) );
    }

    $slug     = $_POST['slug'];
    $locale   = $_POST['locale'];

    $translationsSettings = get_site_option ('wpt_translations' );
    $settings = ( isset( $translationsSettings[ $slug ][ $locale ] ) ) ? $translationsSettings[ $slug ][ $locale ] : false ;

    $data = array(
      'message'  => esc_html__( 'Translations settings loaded', 'wp-translations' ),
      'settings' => $settings,
    );
    wp_send_json_success( $data );

    die();
  }

  public function customUploadDir( $upload_dir ) {

    $upload_dir['subdir'] = '/' . WPTORG_SLUG . '/custom';
    $upload_dir['path']   = $upload_dir['basedir'] . '/' . WPTORG_SLUG . '/custom';
    $upload_dir['url']    = $upload_dir['baseurl'] . '/' . WPTORG_SLUG . '/custom';

    return $upload_dir;
  }

}
