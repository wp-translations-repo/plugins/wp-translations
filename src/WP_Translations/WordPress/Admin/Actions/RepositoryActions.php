<?php

namespace WP_Translations\WordPress\Admin\Actions;

defined( 'ABSPATH' ) or die( 'You don&#8217;t have permission to do this.' );

use WP_Translations\Models\HooksAdminInterface;
use WP_Translations\Models\ActivationInterface;
use WP_Translations\WordPress\Helpers\Helper;
use WP_Translations\WordPress\Helpers\FeatureHelper;
use WP_Translations\WordPress\Helpers\RepositoryHelper;
use WP_Translations\WordPress\Helpers\ModalHelper;
use WP_Translations\WordPress\Async\RepositoryAsyncRequest;
use WP_Translations\APIs\GlotPress_Api;

/**
 * settings Actions
 *
 * @since 1.0.0
 */

class RepositoryActions implements HooksAdminInterface, ActivationInterface {

  public function __construct() {
    $this->repos = ( false !== get_site_option( 'wpt_repositories' ) ) ? get_site_option( 'wpt_repositories' ) : array();
    $this->repositoryUpdate = new RepositoryAsyncRequest();
    $this->isEnable = FeatureHelper::isEnable( 'repositories' );
  }



  /**
   * @see WP_Translations\Models\HooksInterface
   */
  public function hooks() {
    add_action( 'wp_ajax_deleteRepository',     array( $this, 'deleteRepository' ) );
    add_action( 'wp_ajax_activateRepository',   array( $this, 'activateRepository' ) );
    add_action( 'wp_ajax_deactivateRepository', array( $this, 'deactivateRepository' ) );
    add_action( 'wp_ajax_saveRepository',       array( $this, 'saveRepository' ) );
    add_action( 'wp_ajax_getRepoImage',         array( $this, 'getRepoImage' ) );
    add_action( 'wp_ajax_getRepoSettings',      array( $this, 'getRepoSettings' ) );
    add_action( 'wp_ajax_getRepoTree',          array( $this, 'getRepoTree' ) );
    add_action( 'admin_init',                   array( $this, 'updateRepository') );
    if ( false !== $this->isEnable ) {
      add_filter( WPTORG_SLUG . '_pages',  array( $this, 'setPage' ) );
    }
  }

  public function setPage( $pages ) {

    $pages['repositories'] = array(
      'label' => __( 'Repositories', 'wp-translations' ),
      'icon'  => 'dashicons-networking',
      'order' => '20'
    );
    return $pages;
  }

  public function activation() {

    $repositories = array(
      'wordpress' => array(
        'name'        => 'WordPress.ORG',
        'provider'    => 'http',
        'type'        => 'public',
        'token'       => '',
        'url'         => esc_url_raw( 'https://wordpress.org' ),
        'logo'        => WPTORG_PLUGIN_URL .'assets/img/dotorg.png',
        'description' => '',
        'status'      => 'protected',
        'active'      => 1,
      ),
      'wp-translations' => array(
        'name'        => 'WP-Translations ORG',
        'provider'    => 'github',
        'type'        => 'public',
        'token'       => '',
        'url'         => esc_url_raw( 'https://github.com/WP-Translations/language-packs' ),
        'rescue'      => array(
          'provider' => 'gitlab',
          'type'     => 'public',
          'token'    => '',
          'url'      => esc_url_raw( 'https://gitlab.com/wp-translations-repo/lp2' ),
        ),
        'logo'        => WPTORG_PLUGIN_URL .'assets/img/WP-TORG.png',
        'description' => '',
        'status'      => 'protected',
        'active'      => 1,
      ),
      /*'woocommerce' => array(
        'name'        => 'WooCommerce',
        'provider'    => 'glotpress',
        'type'        => 'public',
        'excludes'    => array( '1', '602', '7', '42', '655', '725', '2', '13', '392', '8', '639' ),
        'translated'  => '89',
        'url'         => esc_url_raw( 'https://translate.wordpress.com' ),
        'logo'        => WPTORG_PLUGIN_URL .'assets/img/woocommerce.png',
        'description' => 'WooCommerce Premium Translations',
        'status'      => 'protected',
        'active'      => '',
      ),*/
    );
    add_site_option( 'wpt_repositories', $repositories );

  }

  public function activateRepository() {

    if ( ! wp_verify_nonce( $_POST['nonce'], 'wpt-repository-nonce' ) ) {
      wp_die( esc_html__( 'An error has occurred.', 'wp-translations' ) );
    }

    $slug = esc_attr( $_POST['slug'] );
    $this->repos[ $slug ]['active'] = 1;

    update_site_option( 'wpt_repositories', $this->repos );

    $data = array(
      'message' => esc_html__( 'Repository activated.', 'wp-translations' ),
      'repos'   => $this->repos,
    );

    Helper::clearUpdateCache( 'all' );

    wp_send_json_success( $data );

    die();
  }

  public function deactivateRepository() {

    if ( ! wp_verify_nonce( $_POST['nonce'], 'wpt-repository-nonce' ) ) {
      wp_die( esc_html__( 'An error has occurred.', 'wp-translations' ) );
    }

    $slug = esc_attr( $_POST['slug'] );
    $this->repos[ $slug ]['active'] = '';

    update_site_option( 'wpt_repositories', $this->repos );

    $data = array(
      'message' => esc_html__( 'Repository deactivated.', 'wp-translations' ),
      'repos'   => $this->repos,
    );

    Helper::clearUpdateCache( 'all' );

    wp_send_json_success( $data );

    die();
  }

  public function getRepoSettings() {

    if ( ! wp_verify_nonce( $_POST['nonce'], 'wpt-repository-nonce' ) ) {
      wp_die( esc_html__( 'An error has occurred.', 'wp-translations' ) );
    }

    $slug = $_POST['slug'];

    $repoSettings = get_site_option ('wpt_repositories' );
    $settings     = ( isset( $repoSettings[ $slug ] ) ) ? $repoSettings[ $slug ] : false ;

    $data = array(
      'message'  => esc_html__( 'Repository settings loaded', 'wp-translations' ),
      'settings' => $settings,
      'logo'     => wp_get_attachment_image( absint( $settings['logo'] ), 'medium', false, array( 'style' => 'max-height: 25px; width: auto;', 'alt' => $settings['name'] ) )
    );
    wp_send_json_success( $data );

    die();
  }

  public function saveRepository() {

    if ( ! wp_verify_nonce( $_POST['nonce'], 'wpt-repository-nonce' ) ) {
      wp_die( esc_html__( 'An error has occurred.', 'wp-translations' ) );
    }

    $errors = array();
    $action = $_POST['form_action'];
    $slug   = $_POST['slug'];

    if ( empty( $_POST['name'] ) ) {
      $errors[] = array(
        'field'   => 'repo-name',
        'message' => esc_html__( 'This field is required.', 'wp-translations' ),
      );
    }

    if ( empty( $_POST['url'] ) ) {
      $errors[] = array(
        'field'   => 'repo-url',
        'message' => esc_html__( 'This field is required.', 'wp-translations' ),
      );
    }

    if ( 'http' == $_POST['provider'] && 'private' == $_POST['type'] ) {
      $errors[] = array(
        'field'   => 'repo-type',
        'message' => esc_html__( 'HTTP repository must be public!', 'wp-translations' ),
      );
    }

    if ( 'private' == $_POST['type'] && empty( $_POST['token'] ) ) {
      $errors[] = array(
        'field'   => 'repo-token',
        'message' => esc_html__( 'Token is required for private repository!', 'wp-translations' ),
      );
    }

    $repoSlug     = ( 'edit' != $action ) ? sanitize_title( $_POST['name'] ) : $slug;
    $repoData = array(
      'name'        => sanitize_text_field( $_POST['name'] ),
      'provider'    => sanitize_title( $_POST['provider'] ),
      'type'        => sanitize_title( $_POST['type'] ),
      'token'       => sanitize_text_field( $_POST['token'] ),
      'url'         => esc_url_raw( $_POST['url'] ),
      'logo'        => absint( $_POST['logo'] ),
      'description' => sanitize_text_field( $_POST['description'] ),
      'translated'  => ( isset( $_POST['translated'] ) ) ? absint( $_POST['translated'] ) : '',
      'status'      => '',
      'active'      => (bool) $_POST['active'],
    );

    if ( ! empty( $errors ) ) {

      $data = array(
        'errors'  => $errors,
        'message' => esc_html__( 'Something went wrong', 'wp-translations' ),
        'repo'  => $repoData,
      );
      wp_send_json_error( $data );

    } else {

      $this->repos[ $repoSlug ] = $repoData;
      update_site_option( 'wpt_repositories', $this->repos );

      $repoData['logo'] = wp_get_attachment_image( $repoData['logo'], 'medium', false, array( 'style' => 'max-height: 25px; width: auto;', 'alt' => $_POST['name'] ) );

      $data = array(
       'message' => ( 'add-new' == $action ) ? esc_html__( 'Repository successfully added!', 'wp-translations' ) : esc_html__( 'Settings saved.', 'wp-translations' ),
       'slug'  => $repoSlug,
       'repo'  => $repoData
      );

      wp_send_json_success( $data );
    }

    die();
  }

  public function deleteRepository() {

    if ( ! wp_verify_nonce( $_POST['nonce'], 'wpt-repository-nonce' ) ) {
      wp_die( esc_html__( 'An error has occurred.', 'wp-translations' ) );
    }

    $slug = $_POST['slug'];

    if ( isset( $this->repos[ $slug ] ) ) {
      unset( $this->repos[ $slug ] );
    }
    update_site_option( 'wpt_repositories', $this->repos );
    delete_site_transient( 'wpt_repository_' . $slug );

    $data = array(
     'message' => esc_html__( 'Repository successfully deleted!', 'wp-translations' ),
    );

    Helper::clearUpdateCache( 'all' );

    wp_send_json_success( $data );

    die();
  }


  public static function getRepoImage() {

    if( isset( $_POST['id'] ) ) {
        $image = wp_get_attachment_image( absint( $_POST['id'] ), 'medium', false, array( 'style' => 'max-height: 25px; width: auto;' ) );
        $data = array(
            'image'    => $image,
        );
        wp_send_json_success( $data );
    } else {
        wp_send_json_error();
    }
    die();
  }

  public static function updateRepository() {
    $repos = RepositoryHelper::getAllRepos();
    foreach( $repos as $slug => $repo ) {
      if ( '1' == $repo['active'] ) {
        $transient = get_site_transient( 'wpt_repository_' . $slug );
        if ( false === $transient && 'wordpress' != $slug ) {
          $this->repositoryUpdate->data( array( 'slug' => $slug ) )->dispatch();
        }
      }
    }
  }

  public function getRepoTree() {

    if ( ! wp_verify_nonce( $_POST['nonce'], 'wpt-repository-nonce' ) ) {
      wp_die( esc_html__( 'An error has occurred.', 'wp-translations' ) );
    }

    $repoSlug = $_POST['repo'];
    $repoPath = ( isset( $_POST['path'] ) ) ? $_POST['path'] : '';

    $repo     = $this->repos[ $repoSlug ];
    $url      = ( ! empty( $repoPath ) ) ? RepositoryHelper::getRepoUrl( 'glotpress', $repo['url'] ) . $repoPath : RepositoryHelper::getRepoUrl( 'glotpress', $repo['url'] );
    $projects = GlotPress_Api::getProject( esc_url( $url ) );
    $list = ( isset( $projects->sub_projects ) ) ? $projects->sub_projects : $projects;
    $excludes = ( ! empty ( $repo['excludes'] ) ) ? $repo['excludes'] : array();

    if ( is_object( $projects ) || is_array( $projects ) ) {

      if ( ! empty( $projects->translation_sets ) ) {
        echo '<p><strong>' . esc_html__( 'Translation Sets:&nbsp;' ) . count( $projects->translation_sets ) . '</strong></p>';
      }

      if ( ! empty( $list )  ) {
        echo '<p><strong>' . esc_html__( 'Sub Projects:&nbsp;' ) . count( $list ) . '</strong></p>';
      }

      echo '<ul>';
      foreach ( $list as $project ) :
        $isChecked = ( in_array( $project->id, $excludes ) ) ? ' checked="checked"' : '';
      ?>
        <li class="wpt-repo-list-item">
          <div>
            <input type="checkbox" id="<?php echo absint( $project->id ); ?>" class="repo-excludes" name="repo-excludes[]" value="<?php echo absint( $project->id ); ?>" <?php echo $isChecked; ?> />
            <label for="<?php echo absint( $project->id ); ?>"><?php echo esc_html( $project->name ); ?></label>

            <button class="wpt-button-link wpt-repo-deploy-branch"
                    data-id="<?php echo absint( $project->id ); ?>"
                    data-repo="<?php echo esc_attr( $repoSlug ); ?>"
                    data-project="<?php echo esc_attr( $project->slug ); ?>"
                    data-path="<?php echo esc_attr( $project->path ); ?>">
              <span class="dashicons dashicons-arrow-down"></span>
            </button>

          </div>
          <ul id="wpt-repo-project-sublist-<?php echo absint( $project->id ); ?>" class="wpt-repo-sub-branch hidden"></ul>
        </li>
      <?php endforeach;
      echo '</ul>';

    } else {
      echo $projects;
    }

    die();
  }

}
