<?php

namespace WP_Translations\WordPress\Admin\Actions;

defined( 'ABSPATH' ) or die( 'You don&#8217;t have permission to do this.' );

use WP_Translations\Models\HooksAdminInterface;
use WP_Translations\WordPress\Helpers\FeatureHelper;

/**
 * settings Actions
 *
 * @since 1.0.0
 */

class CapabilityActions implements HooksAdminInterface {

	public function hooks() {
		if( false !== FeatureHelper::isEnable( 'capabilities' ) ) {
			add_filter( WPTORG_SLUG . '_settings_page_actions',      array( $this, 'setTableActions' ) );
			add_filter( WPTORG_SLUG . '_settings_fields',            array( $this, 'setFields' ) );
			add_filter( WPTORG_SLUG . '_settings_tabs',              array( $this, 'setTabs' ) );
			add_filter( WPTORG_SLUG . '_settings_columns',           array( $this, 'setColumns' ), 10 );
		}
	}

	public function setTableActions( $actions ) {
		$actions['capabilities'] = '<div id="wpt-table-actions-capabilities" class="hidden"><button id="wpt-add-cap" type="button" class="wpt-button"><span class="dashicons dashicons-plus-alt"></span> ' . esc_html__( 'Add a new rule', 'wp-translations' ) . '</button></div>';
		return $actions;
	}

	public function setColumns( $columns ) {

		$columns['settings']['capabilities']['option'] = array(
			'label' => __( 'Roles', 'wp-translations' ),
			'class' => '',
			'order' => '0'
		);
		$columns['settings']['capabilities']['description'] = array(
			'label' => __( 'Capabilities', 'wp-translations' ),
			'class' => '',
			'order' => '50'
		);
		$columns['settings']['capabilities']['actions'] = array(
			'label' => __( 'Actions', 'wp-translations' ),
			'class' => 'column-actions',
			'order' => '99'
		);

		return $columns;
	}

	public function setFields( $fields ) {
		global $wp_version;

		if ( $wp_version >= '4.9' ) {
			$fields['features']['capabilities'] = array(
				'label' => __( 'Capabilities', 'wp-translations' ),
				'type'  => 'checkbox',
				'desc'  => __( 'Add translation capabilities (install/update) to specific roles.', 'wp-translations' ),
				'order' => '10'
			);
		}

		return $fields;
	}

	public function setTabs( $tabs ) {
		global $wp_version;

		if ( $wp_version >= '4.9' ) {
			$tabs['settings']['capabilities'] = array(
				'label' => __( 'Capabilities', 'wp-translations' ),
				'icon'  => 'dashicons-shield',
				'order' => '20',
				'class' => 'teaser'
			);
		}

		return $tabs;
	}

}
