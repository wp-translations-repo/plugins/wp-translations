<?php

namespace WP_Translations\WordPress\Admin\Page;

defined( 'ABSPATH' ) or die( 'You don&#8217;t have permission to do this.' );

use WP_Translations\WordPress\Helpers\Helper;
use WP_Translations\WordPress\Helpers\RepositoryHelper;
use WP_Translations\WordPress\Helpers\ModalHelper;

abstract class PageRepositories extends Page {

  public static function setPageActions() {

    $actions  = array();
    $formArgs = array(
      'show_button'      => false,
    );


    $actions['repositories']  = '<button class="js-modal wpt-button wpt-button-repo-settings" data-action="add-new" data-slug="new" data-modal-prefix-class="wpt-modal" data-modal-content-id="wpt-repo-modal" data-modal-close-text="<span>Close modal</span>" data-modal-close-title="Close modal" title="" id="label_modal_1" aria-haspopup="dialog"><span class="dashicons dashicons-plus-alt"></span>  ' . esc_html__( 'Add a Repository', 'wp-translations' ) . '</button>';
    $actions['repositories'] .=  ModalHelper::displayRepoModal( 'new', $formArgs, array() );
    return apply_filters( WPTORG_SLUG . '_repositories_page_actions', $actions );
  }

  public static function setTabs() {

    $tabs['repositories'] = array(
      'repositories' => array(
        'label' => __( 'All', 'wp-translations' ),
        'icon'  => false,
        'order' => '0'
      )
    );

    $tabs = apply_filters( WPTORG_SLUG . '_repositories_tabs', $tabs );
    uasort( $tabs['repositories'], function( $a, $b ) {
      return $a['order'] - $b['order'];
    });

    return $tabs;
  }

  public static function setFields() {

    $repos = ( false !== get_site_option( 'wpt_repositories' ) ) ? get_site_option( 'wpt_repositories' ) : array();

    foreach ( $repos as $slug => $repo ) {
      $fields['repositories']['repositories'][ $slug ] = array(
        'label' => $repo['name'],
        'type'  => 'button',
        'desc'  => $repo['description'],
        'data'  => $repo,
        'slug'  => $slug,
        'order' => '0'
      );
    }

    return apply_filters( WPTORG_SLUG . '_repositories_fields', $fields );
  }

  public static function setColumnsHeaders() {

    $columns = array();
    $tabs    = self::setTabs();

    foreach ( $tabs['repositories'] as $tabKey => $tab ) {

      $columns['repositories'][ $tabKey ] = array(
        'option' => array(
          'label' => __( 'Name', 'wp-translations' ),
          'class' => '',
          'order' => '0'
        ),
        'description' => array(
          'label' => __( 'Description', 'wp-translations' ),
          'class' => '',
          'order' => '50'
        ),
        'type' => array(
          'label' => __( 'Type', 'wp-translations' ),
          'class' => '',
          'order' => '60'
        ),
        'actions' => array(
          'label' => __( 'Actions', 'wp-translations' ),
          'class' => 'column-actions',
          'order' => '99'
        )
      );

      uasort( $columns['repositories'][ $tabKey ], function( $a, $b ) {
        return $a['order'] - $b['order'];
      });

    }

    return apply_filters( WPTORG_SLUG . '_repositories_columns', $columns );
  }

  public static function getColumnsCount( $tab ) {

    $tabs    = self::setTabs();
    $columns = self::setColumnsHeaders();
    $count   = array();

    foreach( array_keys( $tabs['repositories'] ) as $tabKey ) {
      $count[ $tabKey ] = count( $columns['repositories'][ $tabKey ] );
    }

    return $count[ $tab ];
  }

  public static function getColumn_option( $tabKey, $columnID, $fieldID, $field ) {
    $html = '<td>';
    if ( ( 'wp-translations' == $field['slug'] || 'wordpress' == $field['slug'] || 'woocommerce' == $field['slug'] ) && ( ! is_int( $field['data']['logo'] ) ) ) {
      $html .= '<img style="max-height: 25px; width: auto;" src="' . esc_url( $field['data']['logo'] ) . '" alt="' . esc_html( $field['data']['name'] ) . '"/>';
    } elseif ( ! empty( $field['data']['logo'] ) ) {
      $html .= wp_get_attachment_image( absint( $field['data']['logo'] ), 'medium', false, array( 'style' => 'max-height: 25px; width: auto;', 'alt' => $field['data']['name'] ) );

    } else {
      $html .= '<label>' . esc_html( $field['data']['name'] ). '</label>';
    }
    $html .= '</td>';

    return $html;
  }

  public static function getColumn_description( $tabKey, $columnID, $fieldID, $field ) {

    $html  = '<td class="wpt-hide-on-md">';
    $html .= ( 'wp-translations' == $field['slug'] ) ? esc_html__( 'WP-Translations community repository', 'wp-translations' ) : esc_html( $field['data']['description'] );
    $html .= '</td>';

    return $html;
  }

  public static function getColumn_type( $tabKey, $columnID, $fieldID, $field ) {

    $html  = '<td>';
    $html .= '<span class="wpt-repo-status">';
    $html .= esc_html( $field['data']['provider'] ) . '&nbsp;';
    $html .= ( 'public' == $field['data']['type'] ) ? '<span class="dashicons dashicons-admin-site" title="' . esc_html( $field['data']['type'] ) . '"></span>' : '<span class="dashicons dashicons-lock" title="' . esc_html( $field['data']['type'] ) . '"></span>';
    $html .= '<span class="screen-reader-text">' . esc_html( $field['data']['type'] ) . '</span></span>';
    $html .= '</td>';

    return $html;
  }

  public static function getColumn_actions( $tabKey, $columnID, $fieldID, $field ) {

    $html  = '<td class="column-actions">';

      if( 'wordpress' != $field['slug'] ) {

        if ( '1' != $field['data']['active'] ) {
          $html .= '<button id="activate-repo-' . esc_attr( $field['slug'] ) . '" class="wpt-button" data-slug="' . esc_attr( $field['slug'] ) . '"><span class="dashicons dashicons-controls-play"></span> <span class="wpt-hide-on-lg">' . esc_html__( 'Activate', 'wp-translations' ) . '</span></button>';
        } else {
          $html .= '<button id="deactivate-repo-' . esc_attr( $field['slug'] ) . '" class="wpt-button" data-slug="' . esc_attr( $field['slug'] ) . '"><span class="dashicons dashicons-controls-pause"></span> <span class="wpt-hide-on-lg">' . esc_html__( 'Deactivate', 'wp-translations'  ) . '</span></button>';
        }

        if ( 'protected' != $field['data']['status'] ) {
          $editArgs = array(
            'show_button' => false
          );
          $html .= '<button class="js-modal wpt-button wpt-button-repo-settings" data-action="edit" data-slug="' . $field['slug'] . '" data-modal-prefix-class="wpt-modal" data-modal-content-id="wpt-repo-modal" data-modal-close-text="<span>Close modal</span>" data-modal-close-title="Close modal" id="label_modal_1" aria-haspopup="dialog"><span class="dashicons dashicons-admin-tools"></span>  <span class="wpt-hide-on-lg">' . esc_html__( 'Edit', 'wp-translations' ) . '</span></button>';

        }

        $status = array(
          'title'  => ( 'protected' == $field['data']['status'] ) ? __( 'Protected', 'wp-translations' ) : '',
          'button' => ( 'protected' == $field['data']['status'] ) ? 'disabled=disabled' : '',
        );
        $html .= '<button id="delete-repo-' . esc_attr( $field['slug'] ) . '" data-slug="' . esc_attr( $field['slug'] ) . '" class="wpt-button danger wpt-delete-repository" title="' . esc_html( $status['title'] ) . '" ' . esc_attr( $status['button'] ) . '><span class="dashicons dashicons-trash"></span> <span class="screen-reader-text">' . esc_html__( 'Delete', 'wp-translations' ) . '</span></button>';
      }

    $html .= '</td>';

    return $html;
  }

  public static function setPageFooter() {

    $editArgs = array(
      'show_button' => false
    );

    $html = parent::setPageFooter();

    return apply_filters(  WPTORG_SLUG . '_repositories_page_footer', $html );
  }

  public static function setPageDebug() {

    $repos = RepositoryHelper::getAllRepos();

    $debug = array(
      'repos' => array(
        'label' => __( 'Repositories', 'wp-translations' ),
        'data'  => $repos
      )
    );

    return apply_filters(  WPTORG_SLUG . '_repositories_page_debug', $debug );
  }

}
