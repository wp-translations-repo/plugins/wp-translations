<?php

namespace WP_Translations\WordPress\Admin\Page;

defined( 'ABSPATH' ) or die( 'You don&#8217;t have permission to do this.' );

abstract class PageLanguages extends Page {

  public static function setPageActions() {

    $actions = array();
    $availableTranslations = wp_get_available_translations();
    $installedTranslations = get_available_languages();

    $select = '<select id="wpt-dropdown-languages">';
    foreach ( $availableTranslations as $translation ) {
      if ( ! in_array( $translation['language'], $installedTranslations ) ) {
        $select .= '<option value="' . $translation['language'] . '">' . $translation['native_name'] . ' - ' . $translation['english_name'] . ' - ' . $translation['language'] . '</option>';
      }
    }
    $select .= '</select>';

    $actions['languages'] = '<div id="wpt-table-actions-locales">' . $select . ' <button id="wpt-install-language" class="wpt-button" data-colspan="' . self::getColumnsCount( 'languages' ) . '"><span class="dashicons dashicons-plus-alt"></span> ' . esc_html__( 'Install', 'wp-translations' ) . '</button></div>';

    return apply_filters( WPTORG_SLUG . '_languages_page_actions', $actions );
  }

  public static function setTabs() {

    $tabs['languages'] = array(
      'languages' => array(
        'label' => __( 'All', 'wp-transations' ),
        'icon'  => false,
        'order' => '0'
      )
    );

    $tabs = apply_filters( WPTORG_SLUG . '_languages_tabs', $tabs );
    uasort( $tabs['languages'], function( $a, $b ) {
      return $a['order'] - $b['order'];
    });

    return $tabs;
  }

  public static function setFields() {

    $availableTranslations = wp_get_available_translations();
    $installedLanguages    = get_available_languages();
    $fields                = array();

    foreach ( $availableTranslations as $translation ) {
      if ( in_array( $translation['language'], $installedLanguages ) ) {
        $fields['languages']['languages'][ $translation['language'] ] = array(
          'label' => $translation['language'],
          'type'  => 'button',
          'desc'  => $translation['native_name'],
          'english_name' => $translation['english_name'],
          'order' => '0'
        );
      }
    }
    $fields = apply_filters( WPTORG_SLUG . '_languages_fields', $fields );

    return $fields;
  }

  public static function setColumnsHeaders() {

    $columns = array();
    $tabs    = self::setTabs();
    $fields  = self::setFields();

    foreach( $tabs['languages'] as $tabKey => $tab ) {

      $columns['languages'][ $tabKey ] = array(
        'option' => array(
          'label' => __( 'Locales', 'wp-translations' ),
          'class' => '',
          'order' => '0'
        ),
        'description' => array(
          'label' => __( 'Native Names', 'wp-translations' ),
          'class' => '',
          'order' => '50'
        ),
        'english_name' => array(
          'label' => __( 'English Names', 'wp-translations' ),
          'class' => '',
          'order' => '60'
        ),
        'actions' => array(
          'label' => __( 'Actions', 'wp-translations' ),
          'class' => 'column-actions',
          'order' => '99'
        )
      );

      uasort( $columns['languages'][ $tabKey ], function( $a, $b ) {
        return $a['order'] - $b['order'];
      });

    }

    return apply_filters( WPTORG_SLUG . '_languages_columns', $columns );
  }

  public static function getColumnsCount( $tab ) {

    $tabs    = self::setTabs();
    $columns = self::setColumnsHeaders();
    $count   = array();

    foreach( array_keys( $tabs['languages'] ) as $tabKey ) {
      $count[ $tabKey ] = count( $columns['languages'][ $tabKey ] );
    }

    return $count[ $tab ];
  }

  public static function getColumn_option( $tabKey, $columnID, $fieldID, $field ) {

    $td = '<td scope="row">';
      $td .= '<label for="wpt-'. esc_attr( $fieldID ) . '">' . esc_html( $field['label'] ) . '</label>';
      $td .= '<i class="dashicons dashicons-arrow-right"></i>';
    $td .= '</td>';

    return  apply_filters( WPTORG_SLUG . '_languages_column_option', $td, 10 );
  }

  public static function getColumn_description( $tabKey, $columnID, $fieldID, $field ) {
    $td = '<td class="wpt-hide-on-md">';
      $td .= '<span class="description">' . esc_html( $field['desc'] ) . '</span>';
    $td .= '</td>';

    return  apply_filters( WPTORG_SLUG . '_languages_column_description', $td, 10 );
  }

  public static function getColumn_english_name( $tabKey, $columnID, $fieldID, $field ) {
    $td = '<td>' . $field['english_name'] . '</td>';
    return  apply_filters( WPTORG_SLUG . '_languages_column_english_name', $td, 10 );
  }

  public static function getColumn_actions( $tabKey, $columnID, $fieldID, $field ) {
    $td = '<td class="column-actions"><button class="wpt-button danger wpt-delete-language" data-colspan="' . self::getColumnsCount( $tabKey ) . '" data-tab="' . esc_attr( $tabKey ) . '" data-locale="' . esc_attr( $field['label'] ) . '"><span class="dashicons dashicons-trash"></span> ' . esc_html__( 'Delete', 'wp-translations' ) . '</button></td>';
    return $td;
  }

  public static function setPageFooter() {

    $html = parent::setPageFooter();

    return apply_filters(  WPTORG_SLUG . '_languages_page_footer', $html );
  }

  public static function setPageDebug() {

    $availableTranslations = wp_get_available_translations();
    $installedTranslations = get_available_languages();

    $debug = array(
      'available_translations' => array(
        'label' => __( 'Available Translations', 'wp-translations' ),
        'data'  => $availableTranslations
      ),
      'available_languages' => array(
        'label' => __( 'Available Languages', 'wp-translations' ),
        'data'  => $installedTranslations
      )

    );

    return apply_filters(  WPTORG_SLUG . '_languages_page_debug', $debug );
  }

}
