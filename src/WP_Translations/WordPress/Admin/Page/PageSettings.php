<?php

namespace WP_Translations\WordPress\Admin\Page;

defined( 'ABSPATH' ) or die( 'You don&#8217;t have permission to do this.' );

use WP_Translations\WordPress\Helpers\Helper;

abstract class PageSettings extends Page {

  public static function setPageActions() {

    $actions = array();

    return apply_filters( WPTORG_SLUG . '_settings_page_actions', $actions );
  }

  public static function setTabs() {

    $tabs['settings'] = array(
      'features' => array(
        'label' => __( 'Features', 'wp-translations' ),
        'icon'  => 'dashicons-screenoptions',
        'order' => '0'
      ),
      'settings_ui' => array(
        'label'  => __( 'UI Integration', 'wp-translations' ),
        'icon'   => 'dashicons-welcome-view-site',
        'order'  => '10'
      ),
      'languages' => array(
        'label' => __( 'Languages', 'wp-translations' ),
        'icon'  => 'dashicons-flag',
        'order' => '30'
      ),
      'updates' => array(
        'label' => __( 'Updates', 'wp-translations' ),
        'icon'  => 'dashicons-update',
        'order' => '60'
      ),
      'advanced' => array(
         'label' => __( 'Advanced', 'wp-translations' ),
         'icon'  => 'dashicons-admin-generic',
         'order' => '999'
      )
    );

    $tabs = apply_filters( WPTORG_SLUG . '_settings_tabs', $tabs );
    uasort( $tabs['settings'], function( $a, $b ) {
      return $a['order'] - $b['order'];
    });

    return $tabs;
  }

  public static function setFields() {

    $fields['settings'] = array(
      'features' => array(
        'capabilities' => array(
          'label' => __( 'Capabilities', 'wp-translations' ),
          'type'  => 'checkbox',
          'desc'  => __( 'Add translation capabilities (install/update) to specific roles.', 'wp-translations' ),
          'order' => '10',
          'class' => 'teaser'
        ),
        'performance' =>  array(
          'label' => __( 'Performance', 'wp-translations' ),
          'type'  => 'checkbox',
          'desc'  => __( 'Optimize localization.', 'wp-translations' ),
          'order' => '20',
          'class' => 'teaser'
        ),
        'premium' => array(
          'label' => __( 'Premium', 'wp-translations' ),
          'type'  => 'checkbox',
          'desc'  => __( 'Get Premium translation updates and notices.', 'wp-translations' ),
          'order' => '30'
        ),
        'repositories' => array(
          'label' => __( 'Repositories', 'wp-translations' ),
          'type'  => 'checkbox',
          'desc'  => __( 'Allow external repositories for translation updates.', 'wp-translations' ),
          'order' => '40'
        ),
      ),
      'settings_ui' => array(
        'core_updates' => array(
          'label' => __( 'Core updates', 'wp-translations' ),
          'type'  => 'checkbox',
          'desc'  => __( 'Display translation updates details in core\'s update page.', 'wp-translations' ),
          'order' => '10'
        ),
        'plugins_updates' => array(
          'label' => __( 'Plugins updates', 'wp-translations' ),
          'type'  => 'checkbox',
          'desc'  => __( 'Display translation updates details in plugin\'s update page.', 'wp-translations' ),
          'order' => '20'
        ),
        'themes_updates' => array(
          'label' => __( 'Themes updates', 'wp-translations' ),
          'type'  => 'checkbox',
          'desc'  => __( 'Display translation updates details in theme\'s update page.', 'wp-translations' ),
          'order' => '30'
        ),
        'bubble_count' => array(
          'label' => __( 'Display bubble count', 'wp-translations' ),
          'type'  => 'checkbox',
          'desc'  => __( 'Updates bubble count with translations.', 'wp-translations' ),
          'order' => '40'
        ),
        'page_position' => array(
          'label'   => __( 'Page position', 'wp-translations' ),
          'type'    => 'select',
          'choices' => array(
            'menu'    => __( 'Dedicated page', 'wp-translations' ),
            'options' => __( 'Settings subpage', 'wp-translations' ),
          ),
          'desc'    => __( 'Select admin page position', 'wp-translations' ),
          'order' => '50'
        ),
      ),
      'languages' => array(
        'front_user_locale' => array(
          'label' => __( 'Front User Locale', 'wp-translations' ),
          'type'  => 'checkbox',
          'desc'  => __( 'Enable user locale on front-end', 'wp-translations' ),
          'order' => '10'
        ),
        'fallback_language' => array(
          'label' => __( 'Language Fallback', 'wp-translations' ),
          'type'  => 'checkbox',
          'desc'  => __( 'Set a fallback for your chosen language', 'wp-translations' ),
          'order' => '30',
          'class' => 'teaser'
        ),
      ),
      'updates' => array(
        'async_updates' => array(
          'label' => __( 'Async updates', 'wp-translations' ),
          'type'  => 'checkbox',
          'desc'  => __( 'Enable/disable async updates', 'wp-translations' ),
          'order' => '10'
        ),
      ),
      'advanced' => array(
        'debug_mode' => array(
          'label' => __( 'Debug Mode', 'wp-translations' ),
          'type'  => 'checkbox',
          'desc'  => __( 'Toggle debug mode. It may affect performance.', 'wp-translations' ),
          'order' => '10'
        ),
        'force_translations' => array(
          'label' => __( 'Check Translation Updates', 'wp-translations' ),
          'type'  => 'submit',
          'desc'  => __( 'Force translation updates.', 'wp-translations' ),
          'order' => '20'
        ),
        'reset_translations' => array(
          'label' => __( 'Reset Translations', 'wp-translations' ),
          'type'  => 'submit',
          'desc'  => __( 'Reset all translations settings: repositories priorities, locked translations...', 'wp-translations' ),
          'order' => '30'
        ),
        'purge_logs' => array(
          'label' => __( 'Purge Logs', 'wp-translations' ),
          'type'  => 'submit',
          'desc'  => '',
          'order' => '40'
        ),
      ),
    );

    $fields = apply_filters( WPTORG_SLUG . '_settings_fields', $fields );

    foreach( $fields['settings'] as $tabKey => $rows ) {
      uasort( $fields['settings'][ $tabKey ], function( $a, $b ) {
        return $a['order'] - $b['order'];
      });
    }

    return $fields;
  }

  public static function setColumnsHeaders() {

    $columns = array();
    $tabs    = self::setTabs();

    foreach( $tabs['settings'] as $tabKey => $tab ) {

      $columns['settings'][ $tabKey ] = array(
        'option'     => array(
          'label' => __( 'Options', 'wp-translations' ),
          'class' => '',
          'order' => '0'
        ),
        'description' => array(
          'label' => __( 'Description', 'wp-translations' ),
          'class' => '',
          'order' => '50'
        ),
        'actions' => array(
          'label' => __( 'Actions', 'wp-translations' ),
          'class' => 'column-actions',
          'order' => '99'
        )
      );

      uasort( $columns['settings'][ $tabKey ], function( $a, $b ) {
        return $a['order'] - $b['order'];
      });

    }

    return apply_filters( WPTORG_SLUG . '_settings_columns', $columns );;
  }

  public static function getColumnsCount( $tab ) {

    $tabs    = self::setTabs();
    $columns = self::setColumnsHeaders();
    $count   = array();

    foreach( array_keys( $tabs['settings'] ) as $tabKey ) {
      $count[ $tabKey ] = count( $columns['settings'][ $tabKey ] );
    }

    return $count[ $tab ];
  }

  public static function getColumn_option( $tabKey, $columnID, $fieldID, $field ) {

    $td = '<td scope="row" valign="top">';
      $td .= '<label for="wpt-'. esc_attr( $fieldID ) . '">' . esc_html( $field['label'] ) . '</label>';
      $td .= '<i class="dashicons dashicons-arrow-right"></i>';
    $td .= '</td>';

    return  apply_filters( WPTORG_SLUG . '_column_option', $td, 10 );
  }

  public static function getColumn_description( $tabKey, $columnID, $fieldID, $field ) {
    $td = '<td class="wpt-hide-on-md">';
      $td .= '<span class="description">' . esc_html( $field['desc'] ) . '</span>';
    $td .= '</td>';

    return  apply_filters( WPTORG_SLUG . '_column_description', $td, 10 );
  }

  public static function getColumn_actions( $tabKey, $columnID, $fieldID, $field ) {
    $options = Helper::getOptions();
    $td = '<td class="column-actions">';
    switch ( $field['type'] ) {

      case 'checkbox':
        $isChecked = ! empty( $options[ esc_attr( $tabKey ) ][ esc_attr( $fieldID ) ] ) && false !== (bool) $options[ esc_attr( $tabKey ) ][ esc_attr( $fieldID ) ] ?  'checked="cheched"' : '';
        $td .= '<input name="wpt_settings[' . esc_attr( $tabKey ) . '][' . esc_attr( $fieldID ) . ']" id="wpt-' . $fieldID . '" class="switch" type="checkbox" value="1" ' . $isChecked . '/>';
        break;

      case 'select':
        $td .= '<select name="wpt_settings[' . esc_attr( $tabKey ) . '][' . esc_attr( $fieldID ) . ']" id="wpt-' . $fieldID . '">';
        foreach ( $field['choices'] as $value => $label ) {
          $isSelected = ! empty( $options[ esc_attr( $tabKey ) ][ esc_attr( $fieldID ) ] ) && $value == $options[ esc_attr( $tabKey ) ][ esc_attr( $fieldID ) ] ?  'selected="selected"' : '';
          $td .= '<option value="' . esc_attr( $value ) . '" ' . $isSelected . '>' . esc_html( $label ) . '</option>';
        }
        $td .= '</select>';
        break;

      case 'button':
      case 'submit':
        $td .= '<input type="' . $field['type'] . '" id="wpt-' . $fieldID . '" name="wpt_settings[' . esc_attr( $tabKey ) . '][' . esc_attr( $fieldID ) . ']" class="wpt-button" value="' . $field['label'] . '" />';
        break;

    }
    $td .= '</td>';

    return  apply_filters( WPTORG_SLUG . '_column_actions', $td, 10 );
  }

  public static function setPageFooter() {

    $html = '<input type="hidden" name="wpt-action" value="saveSettings"/>
    <input type="hidden" name="wpt-settings-nonce" value="' . wp_create_nonce( 'wpt_settings_nonce' ) . '"/>
    <input type="submit" value="' . esc_html__( 'Save Settings', 'wp-translations' ) . '" class="wpt-button wpt-button-primary"/>';

    return apply_filters(  WPTORG_SLUG . '_settings_page_footer', $html );
  }

  public static function setPageDebug() {

    $options = Helper::getOptions();

    $debug = array(
      'settings' => array(
        'label' => __( 'Settings', 'wp-translations' ),
        'data'  => $options
      )
    );

    return apply_filters(  WPTORG_SLUG . '_settings_page_debug', $debug );
  }

}
