<?php

namespace WP_Translations\WordPress\Admin\Page;

defined( 'ABSPATH' ) or die( 'You don&#8217;t have permission to do this.' );

use WP_Translations\WordPress\Helpers\Helper;
use WP_Translations\WordPress\Helpers\ProductHelper;
use WP_Translations\WordPress\Helpers\LicenseHelper;
use WP_Translations\WordPress\Helpers\TranslationHelper;
use WP_Translations\WordPress\Helpers\ModalHelper;

abstract class PagePremium extends Page {

  public static function setPageActions() {

    $actions  = array();

    return apply_filters( WPTORG_SLUG . '_premium_page_actions', $actions );
  }

  public static function setTabs() {

    $products = ProductHelper::localProducts();
    $currentLocale = Helper::getLocale();

    foreach ( $products as $locale => $store ) {
      if ( ! empty( $store ) ) {
        $tabs['premium'][ $locale ] = array(
          'label'    => esc_attr__( $locale ) . ' - ' . esc_html__( 'Available translations', 'wp-translations' ),
          'icon'     => 'dashicons-translation',
          'selected' => ( $locale == $currentLocale ) ? 'data-selected="1"' : '',
          'order'    => 10
        );
      }
    }

    $tabs['premium']['addons'] = array(
      'label'    => esc_html__( 'Addons', 'wp-translations' ),
      'icon'     => 'dashicons-admin-plugins',
      'selected' => '',
      'order'    => 0,
      'class'    => 'teaser'
    );

    $tabs = apply_filters( WPTORG_SLUG . '_premium_tabs', $tabs );
    uasort( $tabs['premium'], function( $a, $b ) {
      return $a['order'] - $b['order'];
    });

    return $tabs;
  }

  public static function setFields() {

    $products = ProductHelper::localProducts();

    foreach ( $products as $locale => $store ) {
      foreach ( $store as $key => $product ) {
        $fields['premium'][ $locale ][ $key ] = array(
          'label'  => $product['title'],
          'data'   => $product,
          'slug'   => $product['slug'],
          'locale' => $locale,
          'order'  => '0'
        );
      }
    }

    return apply_filters( WPTORG_SLUG . '_premium_fields', $fields );
  }

  public static function setColumnsHeaders() {

    $columns = array();
    $tabs    = self::setTabs();

    foreach ( $tabs['premium'] as $tabKey => $tab ) {

      $columns['premium'][ $tabKey ] = array(
        'option' => array(
          'label' => __( 'Name', 'wp-translations' ),
          'class' => '',
          'order' => '0'
        ),
        'license' => array(
          'label' => __( 'License Key', 'wp-translations' ),
          'class' => '',
          'order' => '20'
        ),
        'activations' => array(
          'label' => __( 'Activations', 'wp-translations' ),
          'class' => '',
          'order' => '40'
        ),
        'status' => array(
          'label' => __( 'Status', 'wp-translations' ),
          'class' => '',
          'order' => '60'
        ),
        'actions' => array(
          'label' => __( 'Actions', 'wp-translations' ),
          'class' => '',
          'order' => '60'
        )
      );

      uasort( $columns['premium'][ $tabKey ], function( $a, $b ) {
        return $a['order'] - $b['order'];
      });

    }

    return apply_filters( WPTORG_SLUG . '_premium_columns', $columns );
  }

  public static function getColumnsCount( $tab ) {

    $tabs    = self::setTabs();
    $columns = self::setColumnsHeaders();
    $count   = array();

    foreach( array_keys( $tabs['premium'] ) as $tabKey ) {
      $count[ $tabKey ] = count( $columns['premium'][ $tabKey ] );
    }

    return $count[ $tab ];
  }

  public static function getColumn_option( $tabKey, $columnID, $fieldID, $field ) {

    $icon  = ( 'theme' == $field['data']['type'] ) ? 'dashicons-admin-appearance' : 'dashicons-admin-plugins';

    $html  = '<td>';
    $html .= '<label for="wpt-license-' . $field['locale'] . '-' . $field['slug'] . '"><span class="dashicons '. esc_attr( $icon ) . ' wpt-hide-on-lg"></span> ' . esc_html( $field['label'] ) . '</label>';
    $html .= '<button class="js-modal wpt-button-link" data-modal-prefix-class="wpt-modal" data-modal-content-id="wpt-readme-' . $field['slug'] . '-' . $field['locale'] . '" data-modal-close-text="<span>' . esc_html__( 'Close Modal', 'wp-translations' ) . '</span>" data-modal-close-title="' . esc_html__( 'Close Modal', 'wp-translations' ) . '" title="' . $field['slug'] . '-' . $field['locale'] . '" id="label_modal_1" aria-haspopup="dialog"><span class="dashicons dashicons-info"></span> </button>';
    $html .= '</td>';

    $modalArgs = array(
      'show_button' => false,
      'active_tab'  => 'description'
    );
    ModalHelper::displayReadmeModal( TranslationHelper::sanitizeTextdomain( $field['slug'] ), $field['locale'], $modalArgs );

    return $html;
  }

  public static function getColumn_license( $tabKey, $columnID, $fieldID, $field ) {

    $options  = Helper::getOptions();
    $license  = isset( $options['licenses'][ $field['locale'] ][ $field['slug'] ] ) ? $options['licenses'][ $field['locale'] ][ $field['slug'] ]['license'] : '';

    $html  = '<td>';
    $html .= '<input type="password" class="" id="wpt-license-' . $field['locale'] . '-' . $field['slug'] . '" value="' . $license . '" />';
    $html .= '</td>';

    return $html;
  }

  public static function getColumn_activations( $tabKey, $columnID, $fieldID, $field ) {

    $options  = Helper::getOptions();

    $html  = '<td class="wpt-hide-on-xl">';
    if(  LicenseHelper::isValid( $field['slug'], $field['locale'] ) ) {
      $options  = Helper::getOptions();
      $licenseData = $options['licenses'][ $field['locale'] ][ $field['slug'] ]['data'];
      $html .= $licenseData->site_count . '/' . $licenseData->license_limit;
    }
    $html .= '</td>';

    return $html;
  }

  public static function getColumn_status( $tabKey, $columnID, $fieldID, $field ) {

    $html = '<td id="wpt-col-status-' . $field['locale'] . '-' . $field['slug'] . '">';
    if( LicenseHelper::isSave( $field['slug'], $field['locale'] ) ) {
      $options = Helper::getOptions();
      $infos   = ! empty( $options['licenses'][ $field['locale'] ][ $field['slug'] ]['data'] ) ? $options['licenses'][ $field['locale'] ][ $field['slug'] ]['data'] : array();
      $html .= LicenseHelper::messageStatus( $infos );
    }
    $html .= '</td>';

    return $html;
  }

  public static function getColumn_actions( $tabKey, $columnID, $fieldID, $field ) {

    $options    = Helper::getOptions();
    $infos      = ! empty( $options['licenses'][ $field['locale'] ][ $field['slug'] ]['data'] ) ? $options['licenses'][ $field['locale'] ][ $field['slug'] ]['data'] : array();
    $license    = isset( $options['licenses'][ $field['locale'] ][ $field['slug'] ] ) ? $options['licenses'][ $field['locale'] ][ $field['slug'] ]['license'] : '';
    $storesData = ProductHelper::STORES;
    $products   = ProductHelper::localProducts();
    $store      = $products[ $field['locale'] ];

    $html = '<td id="wpt-col-actions-' . esc_attr( $field['locale'] ) . '-' . esc_attr( $field['slug'] ) . '" class="column-actions">';

      if ( ! LicenseHelper::isSave( $field['slug'], $field['locale'] ) ) {

        $html .= '<button type="button" class="wpt-button" data-type="' . esc_attr( $field['data']['type'] ) . 's" data-locale="' . esc_attr( $field['locale'] ) . '" data-slug="' . esc_attr( $field['slug'] ) . '" data-name="' . esc_html( $field['label'] ) . '" id="save-license-' . esc_attr( $field['slug'] ) . '" title="' . esc_html__( 'Save license', 'wp-translations' ) . '"><span class="dashicons dashicons-plus-alt"></span> <span class="wpt-hide-on-lg">' . esc_html__( 'Save license', 'wp-translations' ) . '</span></button>';
        $html .= '<a href="' . esc_url( $field['data']['link'] ) . '" class="wpt-button" target="_blank" title="' . esc_html__( 'Get a license', 'wp-translations' ) . '"><span class="dashicons dashicons-admin-network"></span>  <span class="wpt-hide-on-lg">' . esc_html__( 'Get a license', 'wp-translations' ) . '</span></a>';

      } else {

        //Deactivation button
        if ( ! empty( $infos ) && 'valid' == $infos->license ) {
          $html .= '<button type="button" class="wpt-button" data-type="' . esc_attr( $field['data']['type'] ) . 's" data-locale="' . esc_attr( $field['locale'] ) . '" data-slug="' . esc_attr( $field['slug'] ) . '" data-name="' . esc_html( $field['label'] ) . '" id="deactivate-license-' . esc_attr( $field['slug'] ) . '" title="' . esc_html__( 'Deactivate', 'wp-translations' ) . '"><span class="dashicons dashicons-controls-pause"></span> <span class="wpt-hide-on-lg">' . esc_html__( 'Deactivate', 'wp-translations' ) . '</span></button>';
        }

        if ( ! empty( $infos ) && 'valid' != $infos->license ) {
          if ( isset( $infos->error ) ) {

            $upgrade_id = $infos->price_id + 1;
            if ( 'no_activations_left' == $infos->error && $upgrade_id <= $stores[ $locale ][ $product['slug'] ]['pricing'] ) {
              // Upgrade link
              $html .= '<a href="' . esc_url( $storesData[ $field['locale'] ]['url'] ) . esc_attr( $storesData[ $field['locale'] ]['checkout'] ) . '?edd_action=sl_license_upgrade&license_id=' . absint( $infos->license_id ) . '&upgrade_id=' . absint( $upgrade_id ) . '" class="wpt-button" target="_blank" title="' . esc_html__( 'Upgrade', 'wp-translations' ) . '">
                <span class="dashicons dashicons-unlock"></span>  <span class="wpt-hide-on-lg">' . esc_html__( 'Upgrade', 'wp-translations' ) . '</span>
                </a>';
              $html .= '<button type="button" class="wpt-button" data-type="' . esc_attr( $field['data']['type'] ) . 's" data-locale="' . esc_attr( $field['locale'] ) . '" data-slug="' . esc_attr( $field['slug'] ) . '" data-name="' . esc_html( $field['label'] ) . '" id="activate-license-' . esc_attr( $field['slug'] ) . '" title="' . esc_html__( 'Activate', 'wp-translations' ) . '"><span class="dashicons dashicons-controls-play"></span> <span class="wpt-hide-on-lg">' . esc_html__( 'Activate', 'wp-translations' ) . '</span></button>';
            }
          }


          // Renew link
          if ( isset( $infos->error ) && 'expired' == $infos->error || 'expired' == $infos->license ) {
            $html .= '<a href="' . esc_url( $storesData[ $field['locale'] ]['url'] ) . esc_attr( $storesData[ $field['locale'] ]['checkout'] ) . '?edd_license_key=' . $license . '&download_id=' . absint( $product['id'] ) . '" id="wpt-renew-' . esc_attr( $field['locale'] ) . '-' . esc_attr( $field['slug'] ) . '" class="wpt-button" target="_blank" title="' . esc_html__( 'Renew', 'wp-translations' ) . '"><span class="dashicons dashicons-image-rotate"></span> <span class="wpt-hide-on-lg">' . esc_html_e( 'Renew', 'wp-translations' ) . '</span></a>';
            $html .= '<button type="button" class="wpt-button" data-locale="' . esc_attr( $field['locale'] ) . '" data-type="' . esc_attr( $field['data']['type'] ) . 's" data-slug="' . esc_attr( $field['slug'] ) . '" data-name="' . esc_html( $field['label'] ) . '" id="activate-license-' . esc_attr( $field['slug'] ) . '" title="' . esc_html__( 'Activate', 'wp-translations' ) . '"><span class="dashicons dashicons-controls-play"></span> <span class="wpt-hide-on-lg">' . esc_html__( 'Activate', 'wp-translations' ) . '</span></button>';
          }
        }

        // Activation button
        if ( empty( $infos ) || 'site_inactive' == $infos->license  ) {
          $html .= '<button type="button" class="wpt-button" data-locale="' . esc_attr( $field['locale'] ) . '" data-type="' . esc_attr( $field['data']['type'] ) . 's" data-slug="' . esc_attr( $field['slug'] ) . '" data-name="' . esc_html( $field['label'] ) . '" id="activate-license-' . esc_attr( $field['slug'] ) . '" title="' . esc_html__( 'Activate', 'wp-translations' ) . '"><span class="dashicons dashicons-controls-play"></span> <span class="wpt-hide-on-lg">' . esc_html__( 'Activate', 'wp-translations' ) . '</span></button>';
        }

        $html .= '<button type="button" class="wpt-button danger" title="' . esc_html__( 'Delete license', 'wp-translations' ) . '" data-type="' . esc_attr( $field['data']['type'] ) . 's" data-locale="' . esc_attr( $field['locale'] ) . '" data-slug="' . esc_attr( $field['slug'] ) . '" data-name="' . esc_html( $field['label'] ) . '" id="delete-license-' . esc_attr( $field['slug'] ) . '" title="' . esc_html__( 'Delete license', 'wp-translations' ) . '"><span class="dashicons dashicons-dismiss"></span><span class="screen-reader-text">' . esc_html__( 'Delete license', 'wp-translations' ) . '</span></button>';
      }
    $html .= '</td>';

    return $html;
  }

  public static function setPageFooter() {

    $html = parent::setPageFooter();

    return apply_filters(  WPTORG_SLUG . '_premium_page_footer', $html );
  }

  public static function setPageDebug() {

    $options  = Helper::getOptions();
    $products = ProductHelper::localProducts();

    $debug = array(
      'licenses' => array(
        'label' => __( 'Licenses', 'wp-translations' ),
        'data'  => $options['licenses']
      ),
      'products' => array(
        'label' => __( 'Available Products', 'wp-translations' ),
        'data'  => $products
      )
    );

    return apply_filters(  WPTORG_SLUG . '_premium_page_debug', $debug );
  }

}
