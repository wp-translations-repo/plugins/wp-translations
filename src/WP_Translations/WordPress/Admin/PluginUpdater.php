<?php

namespace WP_Translations\WordPress\Admin;

defined( 'ABSPATH' ) or die( 'You don&#8217;t have permission to do this.' );

use WP_Translations\Models\HooksAdminInterface;
use WP_Translations\WordPress\Admin\ReadmeParser;
use WP_Translations\WordPress\Helpers\FileHelper;

/**
 * Plugin Updater
 *
 * @since 1.0.0
 */

class PluginUpdater implements HooksAdminInterface {

  protected $url;
  protected $slug;
  protected $name;

  public function __construct() {
    $this->url  = 'https://gitlab.com/wp-translations-repo/plugins/wp-translations/raw/master/builds/builds.json';
    $this->name = 'wp-translations/wp-translations.php';
    $this->slug = 'wp-translations';
  }

  public function hooks() {
   add_filter( 'pre_set_site_transient_update_plugins',  array( $this, 'checkUpdate' ) );
   add_filter( 'plugins_api',                            array( $this, 'plugins_api_filter' ), 10, 3 );
  }

  public function checkUpdate( $_transient_data ) {

    if ( ! is_object( $_transient_data ) ) {
      $_transient_data = new stdClass;
    }

    $cached = get_site_transient( 'wp_translations_plugin_update' );

    if ( false === $cached ) {
      $version_info = $this->apiRequest();
      set_site_transient( 'wp_translations_plugin_update', $version_info, DAY_IN_SECONDS );
    } else {
      $version_info = $cached;
    }

    if ( false !== $version_info && is_object( $version_info ) && isset( $version_info->new_version ) ) {

      if ( version_compare( WPTORG_VERSION, $version_info->new_version, '<' ) ) {

        // Convert icons as array
        if ( isset( $version_info->icons ) && ! is_array( $version_info->icons ) ) {
          $new_icons = array();
          foreach ( $version_info->icons as $key => $value ) {
            $new_icons[ $key ] = $value;
          }

          $version_info->icons = $new_icons;
        }

        // Get Readme
        $callRepo   = wp_remote_get( "https://gitlab.com/wp-translations-repo/plugins/wp-translations/raw/master/readme.txt" );
        $readmeRepo = wp_remote_retrieve_body( $callRepo );

        FileHelper::putContent( WPTORG_CONTENT_DIR . '/readme.txt', $readmeRepo );
        $parser = new ReadmeParser();
        $data   = $parser->parse_readme( WPTORG_CONTENT_DIR . '/readme.txt' );

        if ( isset( $version_info->sections ) && ! is_array( $version_info->sections ) ) {
          $new_sections = array();
          foreach ( $version_info->sections as $key => $value ) {
            $new_sections[ $key ] = $value;
          }

          $version_info->sections = $new_sections;
        }

        $_transient_data->response[ $this->name ] = $version_info;

      }

      $_transient_data->last_checked           = current_time( 'timestamp' );
      $_transient_data->checked[ $this->name ] = WPTORG_VERSION;

    }

    return $_transient_data;
  }

  public function apiRequest() {

    $cached   = wp_remote_get( $this->url );
    $httpCode = wp_remote_retrieve_response_code( $cached );

    if ( $httpCode != '200' ) {
      $response = $httpCode;
    } else {
      $response = json_decode( wp_remote_retrieve_body( $cached ) );
    }

    return $response;

  }

  /**
   * Updates information on the "View version x.x details" page with custom data.
   *
   * @uses api_request()
   *
   * @param mixed   $_data
   * @param string  $_action
   * @param object  $_args
   * @return object $_data
   */
  public function plugins_api_filter( $_data, $_action = '', $_args = null ) {

    if ( $_action != 'plugin_information' ) {
      return $_data;
    }

    if ( ! isset( $_args->slug ) || ( $_args->slug != $this->slug ) ) {
      return $_data;
    }

    // Get Readme
    $callRepo   = wp_remote_get( "https://gitlab.com/wp-translations-repo/plugins/wp-translations/raw/master/readme.txt" );
    $readmeRepo = wp_remote_retrieve_body( $callRepo );

    FileHelper::putContent( WPTORG_CONTENT_DIR . '/readme.txt', $readmeRepo );
    $parser    = new ReadmeParser();
    $data      = $parser->parse_readme( WPTORG_CONTENT_DIR . '/readme.txt' );
    $transient = get_site_transient( 'wp_translations_plugin_update' );

    $_data->new_version  = $data['new_version'];
    $_data->last_updated = $transient->last_updated;
    $_data->name         = $data['name'];
    $_data->tested       = $data['tested_up_to'];
    $_data->slug         = $data['slug'];
    $_data->homepage     = "https://wp-translations.org";
    $_data->sections     = $data['sections'];
    //$_data->contributors = $data['contributors'];
    $_data->requires     = $data['requires_at_least'];
    $_data->banners      = array(
      'high' => "https://gitlab.com/wp-translations-repo/plugins/wp-translations/raw/master/builds/assets/banner-1544x500.png",
      'low'  => "https://gitlab.com/wp-translations-repo/plugins/wp-translations/raw/master/builds/assets/banner-722x250.png",
    );

    return $_data;
  }

}
