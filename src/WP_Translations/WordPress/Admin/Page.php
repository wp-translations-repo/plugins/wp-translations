<?php

namespace WP_Translations\WordPress\Admin;

defined( 'ABSPATH' ) or die( 'You don&#8217;t have permission to do this.' );

use WP_Translations\Models\HooksAdminInterface;
use WP_Translations\WordPress\Helpers\Helper;
use WP_Translations\WordPress\Helpers\PageHelper;
use WP_Translations\WordPress\Helpers\FeatureHelper;

/**
 * Admin Page
 *
 * @since 1.0.0
 */

class Page implements HooksAdminInterface {

  /**
   * @see WP_Translations\Models\HooksInterface
   */
  public function hooks() {
    add_action( is_multisite() ? 'network_admin_menu' : 'admin_menu', array( $this, 'addMenuPage' ) );
    add_filter( 'update_footer',                                      array( $this, 'updateFooter'), 15 );
    add_filter( 'admin_footer_text',                                  array( $this, 'adminFooterText' ) );
  }

  /**
   * Add options page.
   */
  public function addMenuPage() {

    $options       = Helper::getOptions();
    $pages         = PageHelper::getPages();
    $primaryPage   = PageHelper::PRIMARY_PAGE;
    $capability    = is_multisite() ? 'manage_network' : 'manage_options';

    if ( 'menu' == $options['settings_ui']['page_position'] ) {

      foreach ( $pages as $slug => $page ) {

        if ( $primaryPage == $slug ) {
          $pluginScreen = add_menu_page(
            'WP-Translations',
            $page['label'],
            $capability,
            WPTORG_SLUG,
            array( $this, 'displayPage' ),
            $page['icon']
          );
        } else {
          if ( false === FeatureHelper::isFeature( $slug ) || false !== FeatureHelper::isEnable( $slug ) ) {
            add_submenu_page(
              WPTORG_SLUG,
              $page['label'],
              $page['label'],
              $capability,
              'admin.php?page=' . WPTORG_SLUG . '&wpt-page=' . $slug,
              ''
            );
          }
        }

      }

    } else {
      $pluginScreen = add_options_page(
        'WP-Translations',
        $pages[ PageHelper::PRIMARY_PAGE ]['label'],
        $capability,
        WPTORG_SLUG,
        array( $this, 'displayPage' )
      );

    }
    if ( $pluginScreen ) {
      add_action( 'load-' . $pluginScreen, array( $this, 'helpTabs' ) );
    }
  }

  public function displayPage() {

    require_once  WPTORG_PLUGIN_DIR_TEMPLATES_ADMIN . '/_header.php';

    require_once  WPTORG_PLUGIN_DIR_TEMPLATES_ADMIN . '/page.php';

    require_once  WPTORG_PLUGIN_DIR_TEMPLATES_ADMIN . '/_footer.php';

  }

  public function helpTabs() {
    $screen = get_current_screen();

    $screen->add_help_tab( array(
        'id'      => 'wp-translations-updates-help', // This should be unique for the screen.
        'title'   => __( 'Updates', 'wp-translations' ),
        'content' => '<p>This is the content for the tab.</p>',

    ) );
    $screen->add_help_tab( array(
        'id'      => 'wp-translations-logs-help', // This should be unique for the screen.
        'title'   => __( 'Logs', 'wp-translations' ),
        'content' => '<p>This is the content for the tab.</p>',

    ) );
    $screen->add_help_tab( array(
        'id'      => 'wp-translations-repos-help', // This should be unique for the screen.
        'title'   => __( 'Repositories', 'wp-translations' ),
        'content' => '<p>This is the content for the tab.</p>',

    ) );

  }

  /**
   * Custom footer text left
   *
   * @param  string $text Get default text.
   * @return return filterd text.
   * @since 1.0.0
   */
  function adminFooterText( $text ) {
    $screen = get_current_screen();
    if ( ! in_array( $screen->id, Helper::adminScreen() ) ) {
      return $text;
    } else {
      $link_1 = '<a href="https://wp-translations.org/" target="_blank">WP-Translations ORG</a>';
      $link_2 = '<a target="_blank" href="https://wp-translations.org/support">';
      $link_3 = '</a>';
      return sprintf( esc_html__( 'Visit %1$s website | %2$sContact Support%3$s', 'wp-translations' ), $link_1, $link_2, $link_3 );
    }
  }


  /**
   * Custom footer text right
   *
   * @param  string $text Get default text.
   * @return return filterd text.
   * @since 1.0.0
   */
  public function updateFooter( $text ) {
    $screen = get_current_screen();
    if ( ! in_array( $screen->id, Helper::adminScreen() ) ) {
      return $text;
    } else {
      $translate = sprintf( '<a class="wptpro-footer-link" href="https://www.transifex.com/wp-translations/wp-translations" title="%s"><span class="dashicons dashicons-translation"></span></a>', esc_html__( 'Help us with Translations', 'wp-translations' ) );
      $version = esc_html__( 'Version:&nbsp;', 'wp-translations' ) . WPTORG_VERSION;
      return $translate . $version;
    }
  }


}
