<?php

namespace WP_Translations\WordPress\Helpers;

defined( 'ABSPATH' ) or die( 'You don&#8217;t have permission to do this.' );

abstract class TableHelper {

  public static function getTableActions() {

    $actions = array();

    return apply_filters( WPTORG_SLUG . '_table_actions', $actions );
  }

  public static function getColumnsHeaders() {

    $columns = array();

    $columns = apply_filters( WPTORG_SLUG . '_table_columns_headers', $columns );
    uasort( $columns, function( $a, $b ) {
      return $a['order'] - $b['order'];
    });
    return $columns;
  }

  public static function getColumnsCount() {
    $count = count( self::getTableColumns() );
    return $count;
  }

  public static function getColumn_option( $tabKey, $columnID, $fieldID, $field ) {

    $td = '<td scope="row" valign="top">';
      $td .= '<label for="wpt-'. esc_attr( $fieldID ) . '">' . esc_html( $field['label'] ) . '</label>';
      $td .= '<i class="dashicons dashicons-arrow-right"></i>';
    $td .= '</td>';

    return  apply_filters( WPTORG_SLUG . '_column_option', $td, 10 );
  }

  public static function getColumn_description( $tabKey, $columnID, $fieldID, $field ) {
    $td = '<td class="wpt-hide-on-md">';
      $td .= '<span class="description">' . esc_html( $field['desc'] ) . '</span>';
    $td .= '</td>';

    return  apply_filters( WPTORG_SLUG . '_column_description', $td, 10 );
  }

  public static function getColumn_actions( $tabKey, $columnID, $fieldID, $field ) {
    $options = Helper::getOptions();
    $td = '<td class="column-actions">';
    switch ( $field['type'] ) {

      case 'checkbox':
        $isChecked = ! empty( $options[ esc_attr( $tabKey ) ][ esc_attr( $fieldID ) ] ) && false !== (bool) $options[ esc_attr( $tabKey ) ][ esc_attr( $fieldID ) ] ?  'checked="cheched"' : '';
        $td .= '<input name="wpt_settings[' . esc_attr( $tabKey ) . '][' . esc_attr( $fieldID ) . ']" id="wpt-' . $fieldID . '" class="switch" type="checkbox" value="1" ' . $isChecked . '/>';
        break;

      case 'select':
        $td .= '<select name="wpt_settings[' . esc_attr( $tabKey ) . '][' . esc_attr( $fieldID ) . ']" id="wpt-' . $fieldID . '">';
        foreach ( $field['choices'] as $value => $label ) {
          $isSelected = ! empty( $options[ esc_attr( $tabKey ) ][ esc_attr( $fieldID ) ] ) && $value == $options[ esc_attr( $tabKey ) ][ esc_attr( $fieldID ) ] ?  'selected="selected"' : '';
          $td .= '<option value="' . esc_attr( $value ) . '" ' . $isSelected . '>' . esc_html( $label ) . '</option>';
        }
        $td .= '</select>';
        break;

      case 'button':
      case 'submit':
        $td .= '<input type="' . $field['type'] . '" id="wpt-' . $fieldID . '" name="wpt_settings[' . esc_attr( $tabKey ) . '][' . esc_attr( $fieldID ) . ']" class="wpt-button" value="' . $field['label'] . '" />';
        break;

    }
    $td .= '</td>';

    return  apply_filters( WPTORG_SLUG . '_column_actions', $td, 10 );
  }

}
