<?php

namespace WP_Translations\WordPress\Helpers;

defined( 'ABSPATH' ) or die( 'You don&#8217;t have permission to do this.' );

use WP_Translations\WordPress\Helpers\Helper;

abstract class FeatureHelper {

  public static function isFeature( $feature ) {
    $features  = array(  'repositories', 'premium', 'performance', 'capabilities' );
    $isFeature =  ( ! in_array( $feature, $features ) ) ? false : true;
    return $isFeature;
  }

  public static function isEnable( $feature ) {

    $options = Helper::getOptions();
    $result = ( ! empty( $options['features'][ $feature ] ) ) ? 1 : 0;
    return (bool) $result;
  }

}
