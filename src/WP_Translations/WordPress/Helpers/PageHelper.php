<?php

namespace WP_Translations\WordPress\Helpers;

defined( 'ABSPATH' ) or die( 'You don&#8217;t have permission to do this.' );

abstract class PageHelper {

  const PRIMARY_PAGE = 'translations';

  public static function getPrimaryPage() {
    return apply_filters( WPTORG_SLUG . '_primary_page', self::PRIMARY_PAGE );
  }

  public static function getPages() {

    $pages = array(

      'translations' => array(
        'label' => __( 'Translations', 'wp-translations' ),
        'icon'  => 'dashicons-translation',
        'order' => '0',
      ),
      'languages' => array(
        'label' => __( 'Languages', 'wp-translations' ),
        'icon'  => 'dashicons-flag',
        'order' => '10',
      ),
      'logs' => array(
        'label' => __( 'Logs', 'wp-translations' ),
        'icon'  => 'dashicons-backup',
        'order' => '90',
      ),
      'settings' => array(
        'label' => __( 'Settings', 'wp-translations' ),
        'icon'  => 'dashicons-admin-settings',
        'order' => '99',
      )
    );

    $pages = apply_filters( WPTORG_SLUG . '_pages', $pages );

    uasort( $pages,  function( $a, $b ) {
      return $a['order'] - $b['order'];
    });

    return $pages;

  }

  public static function getPage( $slug ) {
    $pages = self::getPages();
    return $pages[ $slug ];
  }

  public static function getTabs() {

    $tabs = array();

    $tabs = apply_filters( WPTORG_SLUG . '_tabs', $tabs );

    foreach ( $tabs as $key => $tab ) {
      uasort( $tabs[ $key ],  function( $a, $b ) {
        return $a['order'] - $b['order'];
      });
    }

    return $tabs;

  }

  public static function getFields() {

    $fields = array();

    $fields = apply_filters( WPTORG_SLUG . '_fields', $fields );

    foreach ( $fields as $key => $feature ) {
      uasort( $fields[ $key ],  function( $a, $b ) {
        return $a['order'] - $b['order'];
      });
    }


    return $fields;
  }

}
