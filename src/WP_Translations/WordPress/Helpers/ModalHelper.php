<?php

namespace WP_Translations\WordPress\Helpers;

defined( 'ABSPATH' ) or die( 'You don&#8217;t have permission to do this.' );

use WP_Translations\WordPress\Helpers\Helper;
use WP_Translations\WordPress\Helpers\ProductHelper;
use WP_Translations\WordPress\Helpers\RepositoryHelper;
use WP_Translations\APIs\EDD_SL_Api;

/**
 *
 * @author Jerome SADLER
 * @since 1.0.4
 */
abstract class ModalHelper {

  public static function displayReadmeModal( $slug, $locale, $args = array() ) {

    $defaults = array(
      'show_button'      => true,
      'button_css_class' => 'wpt-button-link',
      'button_text'      => __( 'View details', 'wp-translations' ),
      'button_icon'      => '',
      'active_tab'       => 'description'
    );
    $args = wp_parse_args( $args, $defaults );

    $readme    = EDD_SL_Api::getReadme( $slug, $locale );
    $products  = ProductHelper::localProducts();
    $product   = $products[ $locale ][ $slug ];

    $buttonIcon  = ( ! empty( $args['button_icon'] ) ) ? '<span class="dashicons ' . esc_attr( $args['button_icon'] ) . '"></span> <span class="wpt-hide-on-lg">' : '';
    $buttonAfter = ( ! empty( $args['button_icon'] ) ) ? '</span>' : '';
    ?>

    <?php if ( false !== $args['show_button'] ) : ?>
    <button class="js-modal <?php echo esc_attr( $args['button_css_class'] ); ?>"
          data-modal-prefix-class="wpt-modal"
          data-modal-content-id="wpt-readme-<?php echo esc_attr( $slug ); ?>-<?php echo esc_attr( $locale ); ?>"
          data-modal-close-text="<span><?php esc_html_e( 'Close modal', 'wp-translations'); ?></span>"
          data-modal-close-title="<?php esc_html_e( 'Close modal', 'wp-translations'); ?>"
          title="<?php echo esc_attr( $slug ); ?>-<?php echo esc_attr( $locale ); ?>"><?php echo $buttonIcon; ?> <?php echo esc_html( $args['button_text'] ); ?><?php echo $buttonAfter; ?></button>
    <?php endif; ?>

    <div id="wpt-readme-<?php echo esc_attr( $slug ); ?>-<?php echo esc_attr( $locale ); ?>" class="hidden">

      <?php if ( ! empty ( $readme->banners ) ) :
        $banners = unserialize( $readme->banners );
        if( ! empty( $banners['high'] )) :?>
          <img src="<?php echo esc_url( $banners['high'] ); ?>" style="max-width:100%;" />
        <?php else : ?>
          <img src="<?php echo esc_url( $banners['low'] ); ?>" style="max-width:100%;" />
      <?php endif;
       endif;?>

       <div class="inside">
         <ul>
           <?php if ( ! empty( $readme->stable_version ) ) : ?>
             <li><strong><?php esc_html_e( 'Version:', 'wp-translations' ); ?></strong> <?php echo $readme->stable_version; ?> rev(<?php echo strtotime( $readme->po_revision ); ?>)</li>
           <?php endif; ?>
           <?php if ( ! empty( $readme->po_revision ) ) : ?>
             <li><strong><?php esc_html_e( 'Last Update:', 'wp-translations' ); ?></strong> <?php echo date_i18n( get_option( 'date_format' ),strtotime( $readme->po_revision ) ); ?></li>
           <?php endif; ?>
         </ul>
         <?php if( isset( $readme->contributors ) ) : ?>
           <h3><?php esc_html_e( 'Contributors:', 'wp-translations' ); ?></h3>
           <?php foreach ( $readme->contributors as $contributor ) : ?>
             <a href="https://profiles.wordpress.org/<?php echo $contributor; ?>"><?php echo $contributor; ?></a>
          <?php endforeach;
         endif; ?>
       </div>

      <div class="js-tabs inside" data-tabs-disable-fragment="1">
        <ul class="js-tablist" data-hx="h2" data-tabs-prefix-class="wpt-readme">
        <?php if ( isset( $readme ) && ! empty( $readme ) ) :
          $sections = unserialize( $readme->sections );
          foreach ( $sections as $key => $section ) :
            $activeTab = ( $key == $args['active_tab'] ) ? 'data-selected="1"' : '';
            if ( !empty( $section ) ) : ?>
          <li class="js-tablist__item">
            <a href="#<?php echo $key; ?>_<?php echo esc_attr( $locale ); ?>_<?php echo esc_attr( $slug ); ?>" id="label_<?php echo $key; ?>_<?php echo $locale; ?>_<?php echo esc_attr( $slug ); ?>" class="js-tablist__link" <?php echo $activeTab; ?>><?php echo ProductHelper::getReadmeSections( $key ); ?></a>
          </li>
            <?php endif;
          endforeach; ?>
        </ul>

        <?php foreach ( $sections as $key => $section ) :
          if ( !empty( $section ) ) : ?>
            <div id="<?php echo $key; ?>_<?php echo esc_attr( $locale ); ?>_<?php echo esc_attr( $slug ); ?>" class="js-tabcontent">
              <?php print_r( $section ); ?>
              <?php if( 'description' == $key ) : ?>
                <p class="wpt-product-page-link"><a href="<?php echo esc_url( $product['link'] ) ?>" class="wpt-button" target="_blank"><span class="dashicons dashicons-store"></span> <?php esc_html_e( 'Go to Store', 'wp-translations' ); ?></a></p>
              <?php endif; ?>
            </div>
          <?php endif;
        endforeach; ?>
        <?php endif; ?>
      </div>
    </div>
    <?php
  }

  public static function displayTranslationModal( $slug, $locale, $args = array(), $settings ) {

    $defaults = array(
      'show_button'      => true,
      'button_css_class' => 'wpt-button wp-button-translations-settings',
      'button_text'      => __( 'Settings', 'wp-translations' ),
      'button_icon'      => 'dashicons-admin-tools',
      'active_tab'       => ''
    );
    $args = wp_parse_args( $args, $defaults );

    $currentLocale    = Helper::getLocale();
    $buttonIcon       = ( ! empty( $args['button_icon'] ) ) ? '<span class="dashicons ' . esc_attr( $args['button_icon'] ) . '"></span> ' : '';
    $asyncUpdates     = ( ! empty( $settings['async_updates'] ) && ( false !== (bool) $settings['async_updates'] ) ) ? 'checked="checked"'  :  '';
    $repoPriority     = ( ! empty( $settings['repo_priority'] ) ) ? $settings['repo_priority'] : false;
    $lockTranslation  = ( ! empty( $settings['lock_translation'] ) && ( false !== (bool) $settings['lock_translation'] ) ) ? 'checked="checked"'  :  '';
    $useCustom        = ( ! empty( $settings['use_custom'] ) && ( false !== (bool) $settings['use_custom'] ) ) ? 'checked="checked"'  :  '';
    $customFile       = ( ! empty( $settings['custom_translation'] ) ) ?  $settings['custom_translation'] : false;

    if ( false !== $args['show_button'] ) : ?>
    <button class="js-modal <?php echo esc_attr( $args['button_css_class'] ); ?>"
          data-slug = "<?php echo esc_attr( $slug ); ?>"
          data-locale = "<?php echo esc_attr( $locale ); ?>"
          data-modal-prefix-class="wpt-modal"
          data-modal-content-id="wpt-translation-settings-<?php echo esc_attr( $slug ); ?>-<?php echo esc_attr( $locale ); ?>"
          data-modal-close-text="<span><?php esc_html_e( 'Close modal', 'wp-translations'); ?></span>"
          data-modal-close-title="<?php esc_html_e( 'Close modal', 'wp-translations'); ?>"
          title="<?php echo esc_attr( $slug ); ?>-<?php echo esc_attr( $locale ); ?>"><?php echo $buttonIcon; ?> <?php echo $args['button_text']; ?></button>
    <?php endif; ?>

    <?php ob_start(); ?>
    <div id="wpt-translation-settings-<?php echo esc_attr( $slug ); ?>-<?php echo esc_attr( $locale ); ?>" class="hidden">
      <div class="inside wpt-modal-settings">

        <div id="wpt-settings-overlay-<?php echo esc_attr( $slug ); ?>-<?php echo esc_attr( $locale ); ?>"class="wpt-settings-overlay hidden">
          <p><?php esc_html_e( 'Loading settings', 'wp-translations'); ?></p>
        </div>
        <h2><?php echo esc_attr( $slug ); ?> [<?php echo esc_attr( $locale ); ?>]</h2>

          <table class="wpt-settings-table wpt-modal-table">
            <tbody>
              <?php
                $fields = TranslationHelper::getSettingsFields();
                uasort( $fields,  function( $a, $b ) {
                  return $a['order'] - $b['order'];
                });
                foreach ( $fields as $key => $field ) :
                  $isRequired = ( false !== (bool) $field['required'] ) ? '' : 'wpt-required-field';
                  $isVisible  = ( false !== (bool) $field['hidden'] ) ? ' hidden' : '';
                  $isValue    = ( isset( $field['value'] ) && ! empty( $field['value'] ) ) ? $field['value'] : '';
                  $min        = ( isset( $field['min'] ) && ! empty( $field['min'] ) ) ? $field['min'] : '';
                  $max        = ( isset( $field['max'] ) && ! empty( $field['max'] ) ) ? $field['max'] : '';
                ?>

                <tr id="wpt-field-row-translation-<?php echo esc_attr( $key ); ?>-<?php echo esc_attr( $slug ); ?>-<?php echo esc_attr( $currentLocale ); ?>" class="wpt-license-row <?php echo $isVisible; ?>">
                  <td>
                    <label for="translation-<?php echo esc_attr( $key ); ?>-<?php echo esc_attr( $slug ); ?>-<?php echo esc_attr( $currentLocale ); ?>" class="<?php echo $isRequired; ?>"><?php echo esc_html( $field['label'] ); ?></label><i class="dashicons dashicons-arrow-right"></i>
                    <div id="wpt-field-notice-translation-<?php echo esc_attr( $key ); ?>-<?php echo esc_attr( $slug ); ?>-<?php echo esc_attr( $currentLocale ); ?>" class="wpt-field-notice-error hidden"></div>
                  </td>
                  <td class="column-actions">
                  <?php
                    switch ( $field['type'] ) {
                      case 'text':
                      case 'url':
                      case 'password':
                        echo '<input type="' . esc_attr( $field['type'] ) . '" id="translation-' . esc_attr( $key ) . '-' . esc_attr( $slug ) . '-' . esc_attr( $currentLocale ) . '" value="' . esc_html( $isValue ) . '" />';
                        break;

                      case 'number':

                        echo '<input type="' . esc_attr( $field['type'] ) . '" id="translation-' . esc_attr( $key ) . '-' . esc_attr( $slug ) . '-' . esc_attr( $currentLocale ) . '" value="' . esc_html( $isValue ) . '" min="' . absint( $min ) . '" max="' . absint( $max ) . '" />';
                        break;

                      case 'select':
                        ksort( $field['choices'] );
                        echo '<select id="translation-' . esc_attr( $key ) . '-' . esc_attr( $slug ) . '-' . esc_attr( $currentLocale ) . '" data-slug="' . $slug . '">';
                          foreach ( $field['choices'] as $choiceKey => $choice ) {
                            echo '<option value="' . esc_attr( $choiceKey ). '">' . esc_html( $choice ) . '</option>';
                          }
                        echo '</select>';
                        break;

                      case 'checkbox':
                       echo '<input type="' . esc_attr( $field['type'] ) . '" data-slug="' . esc_attr( $slug ) . '" data-locale="' . esc_attr( $currentLocale ) . '" class="switch" id="translation-' . esc_attr( $key ) . '-' . esc_attr( $slug ) . '-' . esc_attr( $currentLocale ) . '" value="1" />';
                        break;

                      case 'textarea':
                        echo '<textarea id="translation-' . esc_attr( $key ) . '-' . esc_attr( $slug ) . '-' . esc_attr( $currentLocale ) . '"></textarea>';
                        break;

                      case 'file':
                        if ( false === $customFile ) {
                          echo '<input type="file" name="translation-' . esc_attr( $key ) . '" id="translation-' . esc_attr( $key ) . '-' . esc_attr( $slug ) . '-' . esc_attr( $currentLocale ) . '" value="" />';
                        } else {
                          echo '<span>' . $customFile['filename'] . '</span> <button class="wpt-delete-custom-file wpt-button danger" data-slug="' . esc_attr( $slug ) . '" data-locale="' . esc_attr( $currentLocale ) . '"><span class="dashicons dashicons-trash"></span> <span class="screen-reader-text">' . esc_html__( 'Delete custom file', 'wp-translations' ) . '</span></button>';
                        }
                        break;
                    }
                  ?>
                  </td>
                </tr>

              <?php endforeach; ?>

              <tr id="wpt-modal-settings-notice-<?php echo esc_attr( $slug ); ?>-<?php echo esc_attr( $locale ); ?>" class="hidden"></tr>
            </tbody>
          </table>
          <div class="wpt-modal-settings-actions">
            <button type="submit" class="wpt-button wpt-translations-settings-submit" data-slug="<?php echo esc_attr( $slug ); ?>" data-locale="<?php echo esc_attr( $locale ); ?>"><?php esc_html_e( 'Save', 'wp-translations' ); ?></button>
          </div>
      </div>
    </div>
    <?php

    $html = ob_get_clean();

    return $html;
  }

  public static function displayRepoModal( $slug = 'new', $args = array(), $settings ) {

    $defaults = array(
      'show_button'      => true,
      'button_css_class' => 'wpt-button',
      'button_text'      => __( 'View details', 'wp-translations' ),
      'button_icon'      => '',
    );
    $args = wp_parse_args( $args, $defaults );

    $buttonIcon     = ( ! empty( $args['button_icon'] ) ) ? '<span class="dashicons ' . esc_attr( $args['button_icon'] ) . '"></span> ' : '';

    if ( false !== $args['show_button'] ) : ?>
    <button class="js-modal <?php echo esc_attr( $args['button_css_class'] ); ?>"
          data-slug = "<?php echo esc_attr( $slug ); ?>"
          data-modal-prefix-class="wpt-modal"
          data-modal-content-id="wpt-repo-modal"
          data-modal-close-text="<span><?php esc_html_e( 'Close modal', 'wp-translations'); ?></span>"
          data-modal-close-title="<?php esc_html_e( 'Close modal', 'wp-translations'); ?>"
          title=""><?php echo $buttonIcon; ?> <?php echo $args['button_text']; ?></button>
    <?php endif; ?>

    <?php ob_start(); ?>
    <div id="wpt-repo-modal" class="hidden">
      <div id="wpt-repo-form" class="inside wpt-modal-settings">

        <div id="wpt-settings-overlay"class="wpt-settings-overlay hidden" style="display: none;">
          <p><?php esc_html_e( 'Loading settings', 'wp-translations'); ?></p>
        </div>

        <h2><?php esc_html_e( 'Add a Repository', 'wp-translations' ); ?></h2>
        <form action="" method="post" enctype="multipart/form-data">

        <table class="wpt-settings-table wpt-modal-table">
          <tbody>

            <?php
              $fields = RepositoryHelper::getSettingsFields();
              uasort( $fields,  function( $a, $b ) {
                return $a['order'] - $b['order'];
              });
              foreach ( $fields as $key => $field ) :
                $isRequired = ( false !== (bool) $field['required'] ) ? '' : 'wpt-required-field';
                $isVisible  = ( false !== (bool) $field['hidden'] ) ? ' hidden' : '';
                $isValue    = ( isset( $field['value'] ) && ! empty( $field['value'] ) ) ? $field['value'] : '';
                $min        = ( isset( $field['min'] ) && ! empty( $field['min'] ) ) ? $field['min'] : '';
                $max        = ( isset( $field['max'] ) && ! empty( $field['max'] ) ) ? $field['max'] : '';
              ?>

              <tr id="wpt-field-row-repo-<?php echo esc_attr( $key ); ?>" class="wpt-license-row <?php echo $isVisible; ?>">
                <td>
                  <label for="repo-<?php echo esc_attr( $key ); ?>" class="<?php echo $isRequired; ?>"><?php echo esc_html( $field['label'] ); ?></label><i class="dashicons dashicons-arrow-right"></i>
                  <div id="wpt-field-notice-repo-<?php echo esc_attr( $key ); ?>" class="wpt-field-notice-error hidden"></div>
                </td>
                <td class="column-actions">
                <?php
                  switch ( $field['type'] ) {
                    case 'text':
                    case 'url':
                    case 'password':
                      echo '<input type="' . esc_attr( $field['type'] ) . '" id="repo-' . esc_attr( $key ) . '" value="' . esc_html( $isValue ) . '" />';
                      break;

                    case 'number':

                      echo '<input type="' . esc_attr( $field['type'] ) . '" id="repo-' . esc_attr( $key ) . '" value="' . esc_html( $isValue ) . '" min="' . absint( $min ) . '" max="' . absint( $max ) . '" />';
                      break;

                    case 'select':
                      ksort( $field['choices'] );
                      echo '<select id="repo-' . esc_attr( $key ) . '" data-slug="' . $slug . '">';
                        foreach ( $field['choices'] as $choiceKey => $choice ) {
                          $selected = ( isset( $field['selected'] ) && $choiceKey == $field['selected'] ) ? 'selected="selected"' : '';
                          echo '<option value="' . esc_attr( $choiceKey ). '" ' . $selected . '>' . esc_html( $choice ) . '</option>';
                        }
                      echo '</select>';
                      break;

                    case 'checkbox':
                     echo '<input type="' . esc_attr( $field['type'] ) . '" class="switch" id="repo-' . esc_attr( $key ) . '" value="1" />';
                      break;

                    case 'textarea':
                      echo '<textarea id="repo-' . esc_attr( $key ) . '"></textarea>';
                      break;

                    case 'file':
                      echo '<span id="wpt-' . esc_attr( $key ) . '-preview" class="wpt-logo-prevew"></span>';
                      echo '<input type="hidden" name="repo-' . esc_attr( $key ) . '" id="repo-' . esc_attr( $key ) . '" value="" />';
                      echo '<input type="button" class="wpt-button wpt-repo-media-select" data-slug="' . esc_attr( $slug ) . '" value="' . esc_attr( 'Select a image', 'wp-translations' ) . '" />';
                      break;
                  }
                ?>
                </td>
              </tr>

            <?php endforeach; ?>

            <tr id="wpt-modal-repo-notice" class="hidden"></tr>
          </tbody>
        </table>
        <div class="wpt-modal-settings-actions">
          <button type="submit" class="wpt-button wpt-repo-submit"><?php esc_html_e( 'Save', 'wp-translations' ); ?></button>
        </div>

        </form>
      </div>
    </div>
    <?php

    $html = ob_get_clean();

    return $html;
  }

}
