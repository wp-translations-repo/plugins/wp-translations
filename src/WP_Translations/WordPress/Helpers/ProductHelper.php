<?php

namespace WP_Translations\WordPress\Helpers;

defined( 'ABSPATH' ) or die( 'You don&#8217;t have permission to do this.' );

use WP_Translations\APIs\EDD_Api;
use WP_Translations\APIs\EDD_SL_Api;
use WP_Translations\WordPress\Helpers\Helper;
use WP_Translations\WordPress\Helpers\LicenseHelper;
use WP_Translations\WordPress\Helpers\TranslationHelper;

/**
 *
 * @author Jerome SADLER
 * @since 1.0.0
 */
abstract class ProductHelper {

  const STORES = array (
    'fr_FR' => array(
      'url'      => 'https://wp-translations.store/fr',
      'checkout' => '/commander/',
      'prefix'   => '-french',
      'locale'   => 'fr_FR',
    ),
    'addons' => array(
      'url'      => 'https://wp-translations.store',
      'checkout' => '/checkout/',
      'prefix'   => '',
      'locale'   => 'addons',
    )
  );

  public static function localProducts() {

    $products = array();

    if ( ! function_exists( 'get_plugins' ) ) {
      require_once ABSPATH . 'wp-admin/includes/plugin.php';
    }

    foreach ( self::STORES as $store ) {

      $remoteProducts = (array) EDD_Api::getProducts( $store );

      foreach( $remoteProducts['products'] as $remoteProduct ) {
        $slug = str_replace( $store['prefix'], '', $remoteProduct->info->slug );
        $products[ $store['locale'] ][ $slug ] = array(
          'slug'    => $slug,
          'id'			=> $remoteProduct->info->id,
          'title'   => $remoteProduct->info->title,
          'link'    => $remoteProduct->info->link,
          'updated' => $remoteProduct->info->modified_date,
          'version' => $remoteProduct->licensing->version,
          'pricing' => count( (array) $remoteProduct->pricing ),
        );
      }

      //local
      $plugins_active = get_plugins();
      $plugins = array();
      foreach ( $plugins_active as $file => $data ) {
        $domains[ $data['TextDomain'] ] = 'plugin';
      }

      $themes = wp_get_themes();
      foreach ( $themes as $key => $theme ) {
        if( ! empty( $theme->get( 'TextDomain' ) ) ) {
          $domains[ $theme->get( 'TextDomain' ) ] = 'theme' ;
        } else {
          $domains[ sanitize_title( $theme->get( 'Name' ) ) ] = 'theme' ;
        }
      }

      foreach ( $products as $locale => $store ) {
        foreach ( $store as $key => $product ) {
          if ( ! in_array( $product['slug'], array_keys( $domains ) ) && 'addons' != $locale )  {
            unset( $products[ $locale ][ $key ] );
          } else {
            $products[ $locale ][ $key ]['type'] = ( 'addons' == $locale ) ? 'addons' : $domains[ $key ];
          }
        }
      }

    }

    return $products;
  }

  public static function isProduct( $slug, $locale ) {

    $products = self::localProducts();

    if ( isset( $products[ $locale ][ $slug ] ) ) {
      return true;
    } else {
      return false;
    }

  }

  public static function getProductInfos( $slug, $locale ) {

    $infos = get_site_option( 'wpt_settings' );

    return $infos['licenses'][ $locale ][ $slug ];
  }

   public static function getReadmeSections( $key ) {

     switch ( $key ) {
       case 'changelog':
         $label = esc_html__( 'Changelog', 'wp-translations' );
         break;

       case 'installation':
         $label = esc_html__( 'Installation', 'wp-translations' );
         break;

       case 'description':
         $label = esc_html__( 'Description', 'wp-translations' );
         break;

       default:
         $label = $key;
         break;
     }

     return $label;

   }

  public static function isToPromote( $slug, $locale ) {

    if ( false !== self::isProduct( $slug, $locale ) && false === LicenseHelper::isValid( $slug, $locale ) ) {
      return true;
    } else {
      return false;
    }

  }

  public static function getProductsUpdates() {

    $options  = Helper::getOptions();
    $products = self::localProducts();
    $updates  = array();

    if( ! empty( $options['licenses'] ) ) {
      foreach ( $options['licenses'] as $locale => $product ) {
        foreach( $product as $slug => $license ) {

          if ( false !== LicenseHelper::isValid( $slug, $locale ) ) {

            $updater      = EDD_SL_Api::getVersion( $slug, $locale );
            $updateSlug   = TranslationHelper::rewriteTextdomain( $slug );

            $update = array(
              'type'            => $products[ $locale ][ $slug ]['type'],
              'slug'            => $updateSlug,
              'language'        => $locale,
              'version'         => trim( $updater->new_version ),
              'updated'         => $updater->po_revision,
              'package'         => $updater->package,
              'autoupdate'      => 1,
              'repo'            => 'wpt-store',
            );

            $updates[] = (object) $update;

          }
        }
      }
    }
    return $updates;
  }

}
