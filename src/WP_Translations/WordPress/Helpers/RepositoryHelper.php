<?php

namespace WP_Translations\WordPress\Helpers;

defined( 'ABSPATH' ) or die( 'You don&#8217;t have permission to do this.' );

use WP_Translations\WordPress\Helpers\Helper;
use WP_Translations\WordPress\Helpers\FileHelper;
use WP_Translations\WordPress\Helpers\TranslationHelper;

/**
 *
 * @author Jerome SADLER
 * @since 1.0.0
 */
abstract class RepositoryHelper {

  public static function getAllRepos() {

    $repos = get_site_option( 'wpt_repositories' );

    return apply_filters( 'wpt_get_all_repositories', $repos );
  }

  public static function getRepoName( $slug ) {

    if ( 'core' == $slug ) {
      $repoName = 'WordPress.ORG';
      $repoLogo = WPTORG_PLUGIN_URL .'assets/img/dotorg.png';
    } elseif ( 'wpt-store' == $slug ) {
      $repoName = 'WPT-Store';
      $repoLogo = WPTORG_PLUGIN_URL .'assets/img/wpt-store.png';
    } elseif ( 'all' == $slug ) {
      $repoName = 'all';
    } elseif ( 'core' != $slug && 'wpt-store' != $slug ) {
      $repos    = self::getAllRepos();
      $repo     = $repos[ $slug ];
      $repoName = $repo['name'];
      $repoLogo = $repo['logo'];
    }

    if ( ! empty( $repoLogo ) && is_int( $repoLogo )  ) {
      $image = wp_get_attachment_image( $repoLogo, 'medium', false, array( 'style' => 'max-height: 25px; width: auto;', 'alt' => $repoName ) );
      return $image;
    } elseif ( ! empty( $repoLogo ) ) {
      $image = '<img src="' . $repoLogo .' " style ="max-height: 25px; max-width: 100px;" alt="' . $repoName . '" />';
      return $image;
    } else {
      return esc_html( $repoName );
    }

  }

  public static function getRepoInfos( $slug ) {

    $repos    = self::getAllRepos();
    $repo     = $repos[ $slug ];
    $response = array();

    if ( 'wordpress' != $slug && '1' == $repo['active'] ) {

      $cached = get_site_transient(  'wpt_repository_' . $slug );

      if ( false === $cached ) {

        if ( 'private' == $repo['type'] ) {
          $url = esc_url_raw( RepositoryHelper::getRepoUrl( $repo['provider'], $repo['url'] ) . 'language-packs.json?private_token=' . $repo['token']  );
        } elseif ( 'glotpress' == $repo['provider'] ) {
          $url = esc_url_raw( RepositoryHelper::getRepoUrl( $repo['provider'], $repo['url'] ) );
        } else {
          $url = esc_url_raw( RepositoryHelper::getRepoUrl( $repo['provider'], $repo['url'] ) . 'language-packs.json' );
        }

        $cached = wp_remote_get( $url );
        $httpCode = wp_remote_retrieve_response_code( $cached );

        if ( $httpCode != '200' ) {

          if ( isset( $repo['rescue'] ) && ! empty( $repo['rescue'] ) ) {

            if ( 'private' == $repo['rescue']['type'] ) {
              $url = esc_url_raw( $repo['rescue']['url'] . 'language-packs.json?private_token=' . $repo['rescue']['token']  );
            } else {
              $url = esc_url_raw( $repo['rescue']['url'] . 'language-packs.json' );
            }

            $cached = wp_remote_get( $url );
            $httpCode = wp_remote_retrieve_response_code( $cached );

            if ( $httpCode != '200' ) {
              $response = $httpCode;
            } else {
              $response = json_decode( wp_remote_retrieve_body( $cached ) );
              set_site_transient( 'wpt_repository_' . $slug, $response , DAY_IN_SECONDS );
            }

          } else {
           $response = $httpCode;
          }

        } else {

          if ( 'glotpress' == $repo['provider'] ) {

            $projects = json_decode( wp_remote_retrieve_body( $cached ) );
            self::getSubProjects( $slug, $url, $projects );

            $subs     = wp_remote_get(  WPTORG_CONTENT_URL_TEMP . '/' . $slug . '.json' );
            $httpCode = wp_remote_retrieve_response_code( $subs );

            if ( '200' == $httpCode ) {
              $allSubs = json_decode( wp_remote_retrieve_body( $subs ), true );

               foreach ( $allSubs as $textdomain => $languages ) {
                 foreach ( $languages as $locale => $translations ) {

                   foreach ( $translations as $key => $update ) {
                       $updateDates[] = strtotime( $update['updated'] );
                   }

                   $lastest = max( $updateDates );

                   foreach ( $translations as $key => $update ) {
                     if( strtotime( $update['updated'] ) != $lastest ) {
                       unset( $translations[ $key ] );
                     }
                   }

                   $response[ $textdomain ][ $locale ] = (object) $update;
                 }
              }

              unlink( WPTORG_CONTENT_PATH_TEMP . '/' . $slug . '.json' );
            }
          } else {
            $response = json_decode( wp_remote_retrieve_body( $cached ) );
          }

          set_site_transient( 'wpt_repository_' . $slug, $response , DAY_IN_SECONDS );
        }

      } else {
        $response = $cached;
      }

    }

    return $response;
  }

  public static function getSubProjects( $slug, $url, $projects ) {

    $repos    = self::getAllRepos();
    $repo     = $repos[ $slug ];
    $excludes = ( ! empty( $repo['excludes'] ) ) ? $repo['excludes'] : array();

    foreach( $projects as $project ) {

      $response = array();
      if ( isset( $project->active ) &&  '1' == $project->active ) {
      if ( ! in_array( $project->id, $excludes ) ) {
        $args = array(
          'httpversion' => '1.1',
          'method'      => 'GET',
          'timeout'     => 120,
        );
        $subCached = wp_remote_get( esc_url( $url . $project->path ), $args );
        $httpCode = wp_remote_retrieve_response_code( $subCached );
        if ( $httpCode == '200' ) {
          $subResponse = json_decode( wp_remote_retrieve_body( $subCached ) );
        }

        $parts = explode( '/', $project->path );
        if ( 1 == count( $parts ) ) {
          $domain = $parts[0];
        } else {
          $domain = $parts[1];
        }

        if ( ! empty( $subResponse->translation_sets ) ) {
          foreach ( $subResponse->translation_sets as $set ) {

            if ( $repo['translated'] < $set->percent_translated ) {
              $update = array(
                'slug'       => $domain,
                'type'       => 'plugin',
                'language'   => $set->wp_locale,
                'locale'     => $set->locale,
                'updated'    => $set->last_modified,
                'version'    => '',
                'repo'       => $slug,
                'package'    => WPTORG_CONTENT_URL_TEMP . '/' . $domain . '/packages/' . $domain . '-' . $set->wp_locale . '.zip',
                'endpoint'   => $project->path,
                'autoupdate' => 1
              );
              $response[ $domain ][ $set->wp_locale ][ $project->id ] = (object) $update;
            }
          }
        }

        if ( ! empty( $response ) ) {
          if ( ! file_exists( WPTORG_CONTENT_PATH_TEMP . '/' . $slug . '.json' ) ) {
            FileHelper::putContent( WPTORG_CONTENT_PATH_TEMP . '/' . $slug . '.json', json_encode( $response ) );
          } else {
            $othersProjects = wp_remote_get( WPTORG_CONTENT_URL_TEMP . '/' . $slug . '.json' );
            $others = (array) json_decode( wp_remote_retrieve_body( $othersProjects ) );
            $allProjects = array_merge_recursive( $others, $response );
            FileHelper::putContent( WPTORG_CONTENT_PATH_TEMP . '/' . $slug . '.json', json_encode( $allProjects ) );
          }
        }

        if ( ! empty( $subResponse->sub_projects ) ) {
          self::getSubProjects( $slug, $url, $subResponse->sub_projects );
        }
      }
    }
    }
  }

  public static function getProjectsUpdates() {

    $repos      = self::getAllRepos();
    $locales    = ! empty( get_available_languages() ) ? get_available_languages() : array( Helper::getLocale() );
    $allUpdates = array();
    $updates    = array();
    $installedDomains   = TranslationHelper::getAllTranslations();
    $localTextdomains   = wp_list_pluck( $installedDomains, 'version', 'textdomain'  );
    $translationOptions = get_site_option( 'wpt_translations' );

    foreach( $repos as $repoSlug => $repo ) {

      $repoInfos = self::getRepoInfos( $repoSlug );
      if ( false !== (bool) $repo['active'] && ! empty( $repoInfos ) ) {
        foreach ( $repoInfos as $slug => $project ) {
            if ( in_array( $slug, array_keys( $localTextdomains ) ) ) {

              foreach( $project as $locale => $languagePack ) {
                if ( in_array( $locale, $locales ) ) {
                  if ( 'glotpress' != $repo['provider'] ) {
                    $languagePack->package = ( 'private' != $repo['type'] ) ? self::getRepoUrl( $repo['provider'], $repo['url'] ) . $slug .'/' . $languagePack->package : self::getRepoUrl( $repo['provider'], $repo['url'] ) . $slug . '/' . $languagePack->package . '?private_token=' . $repo['token'];
                    $languagePack->repo    = $repoSlug;
                    $languagePack->version = $localTextdomains[ $slug ];
                  }
                  $languagePack->slug    = $slug;
                  $languagePack->version = ( isset( $localTextdomains[ $slug ] ) ) ? $localTextdomains[ $slug ] : '';
                  $updates[] = (object) $languagePack;
                }
              }
            }
        }
      }
    }

    return $updates;
  }

  public static function lastest( $arr ) {
    $lastest = max( strtotime( $arr['updated'] ) );
    return $lastest;
  }

  public static function dropdownRepos() {

    $r     = array();
    $repos = self::getAllRepos();

    $r['all'] = esc_html__( 'All', 'wp-translations' );

    foreach ( $repos as $key => $repo ) {
      if ( '1' == $repo['active'] ) {
        $r[ esc_attr( $key ) ] = esc_html( $repo['name'] );
      }
    }

    return $r;
  }

  public static function getRepoUrl( $provider, $repoUrl ) {

    switch ( $provider ) {
      case 'glotpress':
        $url = $repoUrl . '/api/projects/';
        break;

      case 'http':
        $url = $repoUrl;
        break;

      case 'github':
      case 'gitlab':
        $url = $repoUrl . '/raw/master/';
        break;

    }
    return $url;
  }

  public static function dropdownProviders( $repoSlug, $selected ) {

    $providers = array(
      'github'    => 'GitHub',
      'gitlab'    => 'GitLab',
      'http'      => 'HTTP'
    );

    $html = '<select id="repo-provider" data-slug="' . $repoSlug . '">';
    foreach ( $providers as $key => $provider ) {
      $isSelected = ( $key == $selected ) ? ' selected="selected"' : '';
      $html .= '<option value="' . esc_attr( $key ) .'"' . $isSelected . '>' . esc_html( $provider ) . '</option>';
    }
    $html .= '</select>';

    return $html;
  }

  public static function getSettingsFields() {

    $settings = array(
      'name' => array(
        'type'     => 'text',
        'required' => true,
        'label'    => __( 'Name', 'wp-translations' ),
        'order'    => '0',
        'hidden'   => 0
      ),
      'provider' => array(
        'type'     => 'select',
        'required' => true,
        'label'    => __( 'Provider', 'wp-translations' ),
        'order'    => '10',
        'choices'  => array(
          'github'    => 'GitHub',
          'gitlab'    => 'GitLab',
          'http'      => 'HTTP'
        ),
        'hidden'   => 0
      ),
      'type' => array(
        'type'      => 'select',
        'required'  => false,
        'label'     => __( 'Type', 'wp-translations' ),
        'order'    => '20',
        'choices'   => array(
          'public'  => __( 'Public', 'wp-translations' ),
          'private' => __( 'Private', 'wp-translations' ),
        ),
        'condition' => array(
          'provider' => array( 'github', 'gitlab' )
        ),
        'selected'  => 'public',
        'hidden'    => 0
      ),
      'token' => array(
        'type'      => 'password',
        'required'  => true,
        'label'     => __( 'Token', 'wp-translations' ),
        'order'    => '30',
        'condition' => array(
          'type' => array( 'private' )
        ),
        'hidden'    => 1
      ),
      'url' => array(
        'type'     => 'url',
        'required' => true,
        'label'    => __( 'URL', 'wp-translations' ),
        'order'    => '40',
        'hidden'   => 0
      ),
      'logo' => array(
        'type'     => 'file',
        'required' => false,
        'label'    => __( 'Logo', 'wp-translations' ),
        'order'    => '50',
        'hidden'   => 0
      ),
      'description' => array(
        'type'     => 'textarea',
        'required' => false,
        'label'    => __( 'Description', 'wp-translations' ),
        'order'    => '60',
        'hidden'   => 0
      ),
      'active' => array(
        'type'     => 'checkbox',
        'required' => false,
        'label'    => __( 'Activate', 'wp-translations' ),
        'order'    => '70',
        'hidden'   => 0
      )
    );

    return apply_filters( WPTORG_SLUG . '_repository_settings_fields', $settings );

  }

}
