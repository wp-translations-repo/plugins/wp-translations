<?php

namespace WP_Translations\WordPress\Helpers;

defined( 'ABSPATH' ) or die( 'You don&#8217;t have permission to do this.' );

use WP_Translations\WordPress\Helpers\LicenseHelper;

/**
 *
 * @author Jerome SADLER
 * @since 1.0.0
 */
abstract class LoggerHelper {

  public static function log( $license, $action, $serverData, $response = false, $version = false ) {

    $logs = get_site_option( 'wpt_logs' );
    $time = current_time( 'timestamp' );

    switch ( $action ) {
      case 'activate':
        $message = esc_html__( 'Activation', 'wp-translations' );
        $type = 'license';
        $status['class']   = 'success';
        break;
      case 'deactivate':
        $message = esc_html__( 'Deactivation', 'wp-translations' );
        $type = 'license';
        $status['class']   = 'success';
            break;
      case 'save':
        $message = esc_html__( 'Save', 'wp-translations' );
        $type = 'license';
        $status['class']   = 'success';
        break;
      case 'delete':
        $message = esc_html__( 'Delete', 'wp-translations' );
        $type = 'license';
        $status['message'] = esc_html__( 'Deleted', 'wp-translations' );
        $status['class']   = 'success';
        break;
      case 'update':
        $message = esc_html__( 'Updated', 'wp-translations' );
        $type = 'update';
        $status['message'] = esc_html__( 'Success', 'wp-translations' );
        $status['class']   = 'success';
        break;
      case 'update-available':
        $message = esc_html__( 'Update available', 'wp-translations' );
        $type = 'update';
        $status['message'] = esc_html__( 'Waiting', 'wp-translations' );
        $status['class']   = 'waiting';
        break;
    }

    if ( is_object( $response ) ) {
      $status            = LicenseHelper::humanStatus( $response );
    } else {
      $status['message'] = esc_html__( 'Success', 'wp-translations' );
      $status['class']   = 'success';
    }

    if ( 'update' == $action || 'update-available' == $action ) {
      $logs[ $type ][ $action . '-' . $version['po_revision'] ] = array(
        'action'      => $action,
        'textdomain'  => $license,
        'title'       => $message,
        'date'        => $time,
        'server'      => $serverData,
        'response'    => $response,
        'status'      => $status,
        'version'     => $version
      );
    } else {
      $logs[ $type ][ $time ] = array(
        'action'   => $action,
        'license'  => self::maskLicense( $license, null, strlen( $license ) -4 ),
        'title'    => $message,
        'date'     => $time,
        'server'   => $serverData,
        'response' => $response,
        'status'   => $status
      );
    }
    if ( ! empty ( $logs['license'] ) ) {
      krsort( $logs['license'], SORT_NUMERIC );
    }

    update_site_option( 'wpt_logs', $logs );
  }

  public static function maskLicense( $str, $start = 0, $length = null ) {

    $mask = preg_replace ( "/\S/", "*", $str );
    if ( is_null ( $length ) ) {
        $mask = substr ( $mask, $start );
        $str  = substr_replace ( $str, $mask, $start );
    } else {
        $mask = substr ( $mask, $start, $length );
        $str  = substr_replace ( $str, $mask, $start, $length );
    }
    return $str;
  }

}
