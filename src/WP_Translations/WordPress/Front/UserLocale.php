<?php

namespace WP_Translations\WordPress\Front;

defined( 'ABSPATH' ) or die( 'You don&#8217;t have permission to do this.' );

use WP_Translations\Models\HooksFrontInterface;
use WP_Translations\WordPress\Helpers\Helper;

class UserLocale implements HooksFrontInterface {

  public function __construct() {
    $this->options = Helper::getOptions();
  }

  public function hooks() {
		if ( false !== (bool) $this->options['languages']['front_user_locale'] && is_user_logged_in() ) {
      add_filter( 'locale', array( $this, 'getUserLocale' )  );
		}
  }

	public function getUserLocale( $locale ) {

		if ( $userId = get_current_user_id() )	{
			if ( $userLocale = get_user_meta( $userId, 'locale', true ) ) {
				return $userLocale;
			}
		}

		return $locale;
	}

}
