<?php

namespace WP_Translations\Models;

/**
 *
 * @author Jerome Sadler
 *
 */

defined( 'ABSPATH' ) or die( 'You don&#8217;t have permission to do this.' );

interface HooksFrontInterface extends HooksInterface {}
