<?php

namespace WP_Translations\APIs;

/**
 * Classe to communicate with the GlotPress API
 *
 * @author     WP-Translations Team
 * @link       https://wp-translations.org
 * @since      1.0.6
 *
 * @package    WP_Translations
 */

defined( 'ABSPATH' ) or die( 'You don&#8217;t have permission to do this.' );

use WP_Translations\WordPress\Helpers\FileHelper;
use WP_Translations\WordPress\Helpers\RepositoryHelper;
use Gettext\Translations;
use Gettext\Generators;

/**
 * Class from EDD API
 *
 * @since 1.0.6
 */
abstract class GlotPress_Api {

  public static function createPoFile( $update ) {

    $repos  = RepositoryHelper::getAllRepos();
    $repo   = $repos[ $update['repo'] ];
    $url    = RepositoryHelper::getRepoUrl( 'glotpress', $repo['url'] ) . $update['endpoint'];
    $poPath = WPTORG_CONTENT_PATH_TEMP . '/' . $update['slug'] . '/languages/' . $update['slug'] . '-' . $update['language'] . '.po';
    $moPath = WPTORG_CONTENT_PATH_TEMP . '/' . $update['slug'] . '/languages/' . $update['slug'] . '-' . $update['language'] . '.mo';

    if ( ! file_exists( $poPath ) ) {
      $getStrings = self::getPoFile( $url, $update['locale'] );
      //wp_send_json($update);
      if ( ! is_dir( WPTORG_CONTENT_PATH_TEMP . '/' . $update['slug'] ) ) {
        FileHelper::makeDir( WPTORG_CONTENT_PATH_TEMP . '/' . $update['slug'] );
        FileHelper::makeDir( WPTORG_CONTENT_PATH_TEMP . '/' . $update['slug'] . '/languages' );
        FileHelper::makeDir( WPTORG_CONTENT_PATH_TEMP . '/' . $update['slug'] . '/packages' );
      }

      $poFile = FileHelper::putContent( $poPath, $getStrings );
      $translations = Translations::fromPoFile( $poPath );
      $translations->toMoFile( $moPath );

      $zipPath =  WPTORG_CONTENT_PATH_TEMP . '/' . $update['slug'] . '/packages/' . $update['slug'] . '-' . $update['language'] . '.zip';
      $zipFiles = array(
        $poPath,
        $moPath
      );
      $overwrite = ( file_exists( $zipPath ) ) ? true : false ;

      FileHelper::createZip( $zipFiles, $zipPath, $overwrite );
    }
  }

  public static function getPoFile( $url, $locale ) {

    $getFile  = wp_remote_get( $url . '/' . $locale . '/default/export-translations' );
    $httpCode = wp_remote_retrieve_response_code( $getFile );

    if ( $httpCode != '200' ) {
      $response = $httpCode;
    } else {
     $response = wp_remote_retrieve_body( $getFile );
    }

    return $response;
  }

  public static function getProject( $url ) {

    $args = array(
      'httpversion' => '1.1',
      'method'      => 'GET',
      'timeout'     => 120,
    );

    $request = wp_remote_get( esc_url( $url ), $args );
    $httpCode = wp_remote_retrieve_response_code( $request );

    if ( $httpCode == '200' ) {
      $response = json_decode( wp_remote_retrieve_body( $request ) );
    } else {
      $response = $httpCode;
    }

    return $response;
  }

}
